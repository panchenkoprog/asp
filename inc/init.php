<?php

/**
 * Enqueues scripts and styles for main search page.
 */
function asp_enqueue_scripts()
{
    wp_register_script('asp-isotope-script', plugins_url('/js/isotope/isotope.pkgd.min.js', ASP_MAIN_FILE), array('jquery'));
    wp_enqueue_script('asp-isotope-script');

    wp_register_script('asp-pageup-script', plugins_url('/js/pageup/jquery.pageup.js', ASP_MAIN_FILE), array('jquery'));
    wp_enqueue_script('asp-pageup-script');

    wp_register_script('asp-imagesloaded-script', plugins_url('/js/imagesloaded/imagesloaded.pkgd.min.js', ASP_MAIN_FILE), array('jquery'));
    wp_enqueue_script('asp-imagesloaded-script');

    wp_register_script('asp-search-script', plugins_url('/js/search.js', ASP_MAIN_FILE), array('jquery', 'asp-pageup-script', 'asp-imagesloaded-script'), ASP_PLUGIN_VERSION);
    wp_enqueue_script('asp-search-script');

    $asp_search_shopify_link = '';
    if(function_exists('sssp_get_page_permalink_search_shopify_stores'))
    {
        $asp_search_shopify_link = sssp_get_page_permalink_search_shopify_stores();
    }

    $cip_active_plugin = (bool) get_option('cip_active_plugin');

    $arr_js = array(
        //search
        'asp_path_ajax'              => admin_url('admin-ajax.php'),
        'asp_action_search'          => 'asp_get_listing_result',
        'asp_url_no_img'             => plugins_url('/img/no-image-available.jpg', ASP_MAIN_FILE),
        'asp_production'             => ASP_MIXPANEL_PRODUCTION,
        'asp_search_shopify_link'    => $asp_search_shopify_link,
        'asp_search_by_image_page'   => asp_get_page_permalink_search_by_image(),
        'asp_action_change_cookied'  => ($cip_active_plugin ? 'cip_change_cookied' : ''),
        'asp_type_cookied'           => ($cip_active_plugin ? CIP_TYPE_COOKIED_ASP : ''),
        //for search by image uploadKey
        'asp_action_search_by_image_upload_key' => 'asp_get_result_search_by_image_upload_key',
        'asp_nonce'                  => wp_create_nonce( "asp_nonce" ),
        'asp_api_param_name'         => asp_get_api_param_name(),
        'asp_api_key'                => asp_get_api_key(),
        'asp_upload_url'             => asp_get_upload_url()
    );
    wp_localize_script('asp-search-script', 'AspLocalization', $arr_js);

    wp_register_style('asp-search-style', plugins_url('/css/min/search.min.css', ASP_MAIN_FILE), array(), ASP_PLUGIN_VERSION);
    wp_enqueue_style('asp-search-style');

    wp_register_style('asp-font-awesome', plugins_url('/fonts/font-awesome/css/font-awesome.min.css', ASP_MAIN_FILE));
    wp_enqueue_style('asp-font-awesome');
}


/**
 * Enqueues scripts and styles for search by image.
 */
function asp_search_by_image_enqueue_scripts()
{
    wp_register_script('asp-isotope-script', plugins_url('/js/isotope/isotope.pkgd.min.js', ASP_MAIN_FILE), array('jquery'));
    wp_enqueue_script('asp-isotope-script');

    wp_register_script('asp-pageup-script', plugins_url('/js/pageup/jquery.pageup.js', ASP_MAIN_FILE), array('jquery'));
    wp_enqueue_script('asp-pageup-script');

    wp_register_script('asp-imagesloaded-script', plugins_url('/js/imagesloaded/imagesloaded.pkgd.min.js', ASP_MAIN_FILE), array('jquery'));
    wp_enqueue_script('asp-imagesloaded-script');

    wp_register_script('asp-search-by-image-script', plugins_url('/js/search-by-image.js', ASP_MAIN_FILE), array('jquery', 'asp-pageup-script', 'asp-imagesloaded-script'), ASP_PLUGIN_VERSION);
    wp_enqueue_script('asp-search-by-image-script');

    $asp_img_url = isset($_GET['img_url']) ? asp_get_clean_img_url($_GET['img_url']) : '';

    $asp_search_shopify_link = '';
    if(function_exists('sssp_get_page_permalink_search_shopify_stores'))
    {
        $asp_search_shopify_link = sssp_get_page_permalink_search_shopify_stores();
    }

    $cip_active_plugin = (bool) get_option('cip_active_plugin');

    $arr_js = array(
        'asp_path_ajax'             => admin_url('admin-ajax.php'),
        'asp_nonce'                 => wp_create_nonce( "asp_nonce" ),
        'asp_action'                => 'asp_get_result_search_by_image_url',
        'asp_url_no_img'            => plugins_url('/img/no-image-available.jpg', ASP_MAIN_FILE),
        'asp_production'            => ASP_MIXPANEL_PRODUCTION,
        'asp_img_url'               => $asp_img_url,
        'asp_search_shopify_link'   => $asp_search_shopify_link,
        'asp_search_by_image_page'  => asp_get_page_permalink_search_by_image(),
        'asp_action_change_cookied' => ($cip_active_plugin ? 'cip_change_cookied' : ''),
        'asp_type_cookied'          => ($cip_active_plugin ? CIP_TYPE_COOKIED_ASP : ''),
    );
    wp_localize_script('asp-search-by-image-script', 'AspLocalization', $arr_js);

    wp_register_style('asp-search-style', plugins_url('/css/min/search.min.css', ASP_MAIN_FILE), array(), ASP_PLUGIN_VERSION);
    wp_enqueue_style('asp-search-style');

    wp_register_style('asp-font-awesome', plugins_url('/fonts/font-awesome/css/font-awesome.min.css', ASP_MAIN_FILE));
    wp_enqueue_style('asp-font-awesome');
}


/**
 * Enqueues scripts and styles for search by image.
 */
function asp_product_reveal_search_by_image_enqueue_scripts()
{
    $asp_pr_url = isset($_GET['pr_url']) ? $_GET['pr_url'] : '';

    $arr_js = array(
        'asp_path_ajax'            => admin_url('admin-ajax.php'),
        'asp_pr_url'               => $asp_pr_url,
        'asp_production'           => ASP_MIXPANEL_PRODUCTION,
        'asp_search_by_image_page' => asp_get_page_permalink_search_by_image()
    );

    //wds-script == path...theme...main.js
    wp_localize_script('wds-script', 'AspLocalization', $arr_js);
}



/**
 * @param $original_template
 * @return string
 */
function asp_template_include( $original_template )
{
    $asp_active_plugin = (bool) get_option('asp_active_plugin');

    if ( $asp_active_plugin )
    {
        global $post;

        $val = get_option('asp_option_names');
        $asp_page_output = $val ? $val['asp_page_output'] : null;
        $asp_page_search_by_image = $val ? $val['asp_page_search_by_image'] : null;
        $asp_product_reveal_page = $val ? $val['asp_product_reveal_page'] : null;

        if($post->ID == $asp_page_output)
        {
            add_action( 'wp_enqueue_scripts', 'asp_enqueue_scripts' );
            $path = plugin_dir_path( ASP_MAIN_FILE ). 'templates/page.php';
            return $path;
        }
        elseif($post->ID == $asp_page_search_by_image)
        {
            add_action( 'wp_enqueue_scripts', 'asp_search_by_image_enqueue_scripts' );
            $path = plugin_dir_path( ASP_MAIN_FILE ). 'templates/page-search-by-image.php';
            return $path;
        }
        elseif($post->ID == $asp_product_reveal_page)
        {
            add_action( 'wp_enqueue_scripts', 'asp_product_reveal_search_by_image_enqueue_scripts' );
            return $original_template;
        }
        else
        {
            return $original_template;
        }
    }
    else
    {
        return $original_template;
    }
}
add_filter( 'template_include', 'asp_template_include' );
