<?php

/**
 * Init curl
 *
 * @param $ch
 * @param $url
 * @param $params
 * @param $http_headers
 */
function asp_init_setopt_array_curl(& $ch, $url, $params, $http_headers)
{
    curl_setopt_array($ch, array(
        CURLOPT_URL            => $url,
        CURLOPT_HTTPHEADER     => $http_headers,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "",//Supported encodings are "identity", "deflate" and "gzip". If an empty string is passed, "", a header is sent containing all supported encoding types.
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 60,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_CUSTOMREQUEST  => "POST",
        CURLOPT_POSTFIELDS     => json_encode($params),
        CURLOPT_HEADER         => 1
    ));
}

/**
 * Main Curl query
 *
 * @param $url
 * @param $params
 * @param $http_headers
 * @param bool $realtime
 * @return array
 */
function asp_curl_query($url, $params, $http_headers, $realtime=true)
{
//    $url = "https://api.aliseeks.com/v1/search";
//
//    $options = get_option('asp_option_names');
//    $api_key = (isset($options['asp_api_key']) && $options['asp_api_key']) ? $options['asp_api_key'] : '';
//    $limit   = (isset($options['asp_pagination']) && $options['asp_pagination']) ? $options['asp_pagination'] : ASP_NORMAL_PAGING_PRODUCTS;
//    $limit   = intval($limit);
//    $limit   = $limit <= ASP_MAX_PAGING_PRODUCTS ? $limit : ASP_MAX_PAGING_PRODUCTS;
//    $skip    = intval($step) * $limit;
//
//    $add_params = asp_get_addition_query_params();
//    $params = array(
//        'text'  => $keywords,
//        'skip'  => $skip,
//        'limit' => $limit
//    );
//
//    if(!empty($add_params))
//    {
//        $params = array_merge($params, $add_params);
//    }

//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//    curl_setopt($ch, CURLOPT_ENCODING, "");//Supported encodings are "identity", "deflate" and "gzip". If an empty string is passed, "", a header is sent containing all supported encoding types.
//    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
//    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
//    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//    curl_setopt($ch, CURLOPT_HEADER, 1);
//    curl_setopt($ch, CURLOPT_HTTPHEADER, $http_headers);
//    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
//    curl_setopt($ch, CURLOPT_URL, $url);

    $ch = curl_init();
    asp_init_setopt_array_curl($ch, $url, $params, $http_headers);

    $response = curl_exec($ch);

    if (curl_errno($ch))
    {
        //errors
        $errno = curl_errno($ch);
        $error = curl_error($ch);
        $result = array(
            'ok'    => false,
            'error' => array('code'=>$errno, 'msg'=>$error)
        );
    }
    else
    {
        //response
        $headers = asp_get_array_headers_from_curl_response($ch, $response);
        asp_save_params_remaining_requests($headers, $realtime);

        $response_code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);

        if($response_code == 200)
        {
            $body = asp_get_body_from_curl_response($ch, $response);
            $json = @json_decode($body, true);

            if(empty($json)){
                $json = array();
            }else{
                if(function_exists('cip_update_asp_items')){
                    cip_update_asp_items($json);
                }
            }
            $result = array_merge(array('ok'=> true), $json);
        }
        else
        {
            $error = asp_get_errors_from_curl_headers_response($headers);
            if($error){
                $error = ( asp_get_errors_by_response_code($response_code) ) . "\r\n" . $error;
            }else{
                $error = asp_get_errors_by_response_code($response_code);
            }

            //$error = asp_get_errors_by_response_code($response_code);
            $result = array(
                'ok'    => false,
                'error' => array('code'=>$response_code, 'msg'=>$error)
            );
        }
    }

    curl_close($ch);
    return $result;
}


/**
 * Get addition query params
 *
 * @return array
 */
function asp_get_addition_query_params()
{
    $params = array();
    $sort = 'BEST_MATCH';
    $sort_direction = 'ASC';

    if(isset($_REQUEST['sort']))
    {
        $sort = explode(':', $_REQUEST['sort']);

        if($sort[1] == 'des')
        {
            $sort_direction = 'DESC';
        }

        switch ($sort[0]){
            case 'b':{
                $sort = 'BEST_MATCH';
                break;
            }
            case 'ir':{
                $sort = 'ITEM_RATING';
                break;
            }
            case 'w':{
                $sort = 'WHOLESALE_PRICE';
                break;
            }
            case 'o':{
                $sort = 'ORDERS';
                break;
            }
        }
    }

    $params['sort'] = $sort;
    $params['sortDirection'] = $sort_direction;

    //price

    if(isset($_REQUEST['price_min']) && $_REQUEST['price_min'] != '')
    {
        $params['unitPriceRange']['from'] = $_REQUEST['price_min'];
    }
    if(isset($_REQUEST['price_max']) && $_REQUEST['price_max'] != '')
    {
        $params['unitPriceRange']['to'] = $_REQUEST['price_max'];
    }

    //rating

    if(isset($_REQUEST['rating_min']) && $_REQUEST['rating_min'] != '')
    {
        $params['ratingsRange']['from'] = $_REQUEST['rating_min'];
    }
    if(isset($_REQUEST['rating_max']) && $_REQUEST['rating_max'] != '')
    {
        $params['ratingsRange']['to'] = $_REQUEST['rating_max'];
    }

    //quantity

    if(isset($_REQUEST['quantity_min']) && $_REQUEST['quantity_min'] != '')
    {
        $params['quantityRange']['from'] = $_REQUEST['quantity_min'];
    }
    if(isset($_REQUEST['quantity_max']) && $_REQUEST['quantity_max'] != '')
    {
        $params['quantityRange']['to'] = $_REQUEST['quantity_max'];
    }

    //order

    if(isset($_REQUEST['order_min']) && $_REQUEST['order_min'] != '')
    {
        $params['orderRange']['from'] = $_REQUEST['order_min'];
    }
    if(isset($_REQUEST['order_max']) && $_REQUEST['order_max'] != '')
    {
        $params['orderRange']['to'] = $_REQUEST['order_max'];
    }

    return $params;
}


/**
 * cUrl query get listing result
 *
 * @param $keywords
 * @param $step
 * @return array
 */
function asp_curl_query_get_listing_result($keywords, $step)
{
    $url = "https://api.aliseeks.com/v1/search"; //not Realtime
    $realtime = false;                           //not Realtime

    $options = get_option('asp_option_names');
    $api_key = (isset($options['asp_api_key']) && $options['asp_api_key']) ? $options['asp_api_key'] : '';
    $limit   = (isset($options['asp_pagination']) && $options['asp_pagination']) ? $options['asp_pagination'] : ASP_NORMAL_PAGING_PRODUCTS;
    $limit   = intval($limit);
    $limit   = $limit <= ASP_MAX_PAGING_PRODUCTS ? $limit : ASP_MAX_PAGING_PRODUCTS;
    $skip    = intval($step) * $limit;

    $add_params = asp_get_addition_query_params();
    $params = array(
        'text'  => $keywords,
        'skip'  => $skip,
        'limit' => $limit
    );

    if(!empty($add_params))
    {
        $params = array_merge($params, $add_params);
    }

    $http_headers = array(
        'Content-Type: application/json',
        'X-Api-Client-Id: '.$api_key,
        'cache-control: no-cache'
    );

    $result = asp_curl_query($url, $params, $http_headers, $realtime);
    return $result;
}


/**
 * Get body from cURL response
 *
 * @param $ch
 * @param $response
 * @return bool|string
 */
function asp_get_body_from_curl_response($ch, $response)
{
    // Get the response headers as string
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    return substr($response, $header_size);
}


/**
 * Get array headers from cURL response
 *
 * @param $ch
 * @param $response
 * @return array
 */
function asp_get_array_headers_from_curl_response($ch, $response)
{
    $headers = array();

    // Get the response headers as string
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

    $header = trim( substr($response, 0, $header_size) );

    $array_headers = explode(PHP_EOL, $header);

    // Get the substring of the headers and explode as an array by \r\n
    // Each element of the array will be a string `Header-Key: Header-Value`
    // Retrieve this two parts with a simple regex `/(.*?): (.*)/`
    foreach ($array_headers as $row)
    {
        if(preg_match('/(.*?): (.*)/', $row, $matches)){
            $headers[$matches[1]] = $matches[2];
        }
    }

    return $headers;
}


/**
 * Get errors from cURL headers response
 *
 * @param $array_headers
 * @return string|bool    - return (string) message or (bool) FALSE
 */
function asp_get_errors_from_curl_headers_response($array_headers)
{
    $key_error_detail = 'x-error-detailed-message';
    if(array_key_exists($key_error_detail, $array_headers)){
        return $array_headers[$key_error_detail];
    }else{
        return false;
    }
}


/**
 * Save params:
 * - asp_remaining_requests
 *
 * @param $headers
 * @param bool $realtime
 */
function asp_save_params_remaining_requests($headers, $realtime=true)
{
    $tmp_remaining = 'x-remaining-requests';

    $options = get_option('asp_option_names');
    $change = false;

    foreach ($headers as $key => $val)
    {
        if($key == $tmp_remaining)
        {
            $change = true;
            if($realtime){
                $options['asp_remaining_requests_realtime'] = $val;
            }else{
                $options['asp_remaining_requests'] = $val;
            }
        }
    }

    if($change){
        update_option('asp_option_names', $options, 'no');
    }
}


/**
 * Get errors from cURL response by code
 *
 * @param $code
 * @return string
 */
function asp_get_errors_by_response_code($code)
{
    $errors = array(
        403 => 'Forbidden access (may not have access to an API)',
        422 => 'Problem with the request body',
        429 => 'Exceeded the allowable call count (upgrade your subscription)',
        500 => 'Internal server problem (contact us if this persists)',
    );

    if(array_key_exists($code, $errors)){
        return $errors[$code];
    }else{
        return 'Unknown error!';
    }
}


/**
 * AJAX - get listing result
 */
function asp_get_listing_result()
{
    if(isset($_REQUEST['keywords']) && isset($_REQUEST['step']))
    {
        $asp_active_plugin = (bool) get_option('asp_active_plugin');

        if( $asp_active_plugin )
        {
            if(empty($_REQUEST['keywords']))
            {
                $response = array('ok'=>false, 'error'=>array('msg'=>'Bad request! No keywords.'));
            }
            else
            {
                $response = asp_curl_query_get_listing_result($_REQUEST['keywords'], $_REQUEST['step']);
            }
        }
        else
        {
            $response = array('ok'=>false, 'error'=>array('msg'=>'Plugin not active!'));
        }
    }
    else
    {
        $response = array('ok'=>false, 'error'=>array('msg'=>'Bad request!'));
    }

    //header("Content-type:application/json");
    echo json_encode($response);
    wp_die();
}
add_action('wp_ajax_asp_get_listing_result', 'asp_get_listing_result');
add_action('wp_ajax_nopriv_asp_get_listing_result', 'asp_get_listing_result');



// -------------------------------------- Search by image -------------------------------------------- //


/**
 * Get clean image URL
 *
 * @param $url
 * @return string
 */
function asp_get_clean_img_url($url)
{
    if($url){
        return asp_unparse_url(parse_url($url));
    }else{
        return '';
    }
}


/**
 * Conversion back to string from a parsed url
 *
 * @param $parsed_url
 * @return string
 */
function asp_unparse_url($parsed_url)
{
    $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
    $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
    $pass     = ($user || $pass) ? "$pass@" : '';
    $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';

    return "$scheme$user$pass$host$port$path";
}


/**
 * Get page permalink to Search by image
 *
 * @return string
 */
function asp_get_page_permalink_search_by_image()
{
    $options = get_option('asp_option_names');
    $val = ($options && $options['asp_page_search_by_image']) ? $options['asp_page_search_by_image'] : null;
    if($val){
        return get_permalink($val);
    }else{
        return '';
    }
}


/**
 * Get API param name
 * @return string
 */
function asp_get_api_param_name()
{
    return 'X-Api-Client-Id';
}

/**
 * Get API Key
 * @return null|string
 */
function asp_get_api_key()
{
    $options = get_option('asp_option_names');
    $val = ($options && $options['asp_api_key']) ? $options['asp_api_key'] : null;
    if($val){
        return $val;
    }else{
        return '';
    }
}


/**
 * Get upload URL
 * @return string
 */
function asp_get_upload_url()
{
    return "https://api.aliseeks.com/v1/search/image"; //Realtime
}


/**
 * cUrl query get uploadKey by image Url
 *
 * @param $img_url
 * @return array
 */
function asp_curl_query_get_uploadKey_by_image_url($img_url)
{
    $url = "https://api.aliseeks.com/v1/search/image/upload"; //Realtime

    $options = get_option('asp_option_names');
    $api_key = (isset($options['asp_api_key']) && $options['asp_api_key']) ? $options['asp_api_key'] : '';

    $params = array(
        'url' => $img_url,
    );

    $http_headers = array(
        'Content-Type: application/json',
        'X-Api-Client-Id: '.$api_key,
        'cache-control: no-cache'
    );

    $result = asp_curl_query($url, $params, $http_headers);
    return $result;
}


/**
 * cUrl query get result by uploadKey
 *
 * @param $uploadKey
 * @return array
 */
function asp_curl_query_get_result_by_uploadKey($uploadKey)
{
    $url = "https://api.aliseeks.com/v1/search/image"; //Realtime

    $options = get_option('asp_option_names');
    $api_key = (isset($options['asp_api_key']) && $options['asp_api_key']) ? $options['asp_api_key'] : '';

    $params = array(
        'uploadKey' => $uploadKey,
    );

    $http_headers = array(
        'Content-Type: application/json',
        'X-Api-Client-Id: '.$api_key,
        'cache-control: no-cache'
    );

    $result = asp_curl_query($url, $params, $http_headers);
    return $result;
}


/**
 * Run multi curl query
 *
 * @param $items
 * @return array
 */
function asp_multi_curl_query($items)
{
    // initialization "container" for separate connections (multi curl)
    $cmh = curl_multi_init();

    // array of tasks for multi curl
    $tasks = array();
    // iterating over our urls
    foreach ($items as $key=>$item)
    {
        // initialization separate connection (stream)
        $ch = curl_init();
        asp_init_setopt_array_curl($ch, $item['url'], $item['params'], $item['http_headers']);
        // add stream descriptor to task array
        $tasks[$key] = $ch;
        // add stream handle to multi curl
        curl_multi_add_handle($cmh, $ch);
    }

    // number of active streams (threads)
    $active = null;
    // run execution of flows
    do {
        $mrc = curl_multi_exec($cmh, $active);
    }
    while ($mrc == CURLM_CALL_MULTI_PERFORM);

    // we execute while there are active threads
    while ($active && ($mrc == CURLM_OK))
    {
        // if any thread is ready for action
        if (curl_multi_select($cmh) == -1) {
            //http://php.net/manual/ru/function.curl-multi-select.php#115381
            usleep(100); // wait for something to change
        }

        do {
            $mrc = curl_multi_exec($cmh, $active);
            // get info about stream
            $info = curl_multi_info_read($cmh);
            // if the stream has ended
            if ($info['msg'] == CURLMSG_DONE) {
                $ch = $info['handle'];
                // We are looking for the URL of the page in the flow descriptor in the array of tasks
                $key = array_search($ch, $tasks);

                // pick up the contents

                //old default
                //$tasks[$item] = curl_multi_getcontent($ch);

                //custom
                $tasks[$key] = array(
                    'content'     => curl_multi_getcontent($ch),
                    'http_code'   => curl_getinfo($ch, CURLINFO_HTTP_CODE),
                    'header_size' => curl_getinfo($ch, CURLINFO_HEADER_SIZE)
                );

                // remove stream from multi curl
                curl_multi_remove_handle($cmh, $ch);
                // close a separate connection (stream)
                curl_close($ch);
            }
        }
        while ($mrc == CURLM_CALL_MULTI_PERFORM);
    }

    //custom
    //destroy unfulfilled cUrl recourse!
    foreach ($tasks as $key=>$val)
    {
        if(is_resource($tasks[$key]))
        {
            $ch = $tasks[$key];
            curl_multi_remove_handle($cmh, $ch);
            curl_close($ch);
            $tasks[$key] = null;
        }
    }

    // close multi curl
    curl_multi_close($cmh);

    return $tasks;
}


/**
 * Get array headers from multi cURL response
 *
 * @param $response
 * @return array
 */
function asp_get_array_headers_from_multi_curl_response($response)
{
    $headers = array();

    $header = trim( substr($response['content'], 0, $response['header_size']) );

    $array_headers = explode(PHP_EOL, $header);

    // Get the substring of the headers and explode as an array by \r\n
    // Each element of the array will be a string `Header-Key: Header-Value`
    // Retrieve this two parts with a simple regex `/(.*?): (.*)/`
    foreach($array_headers as $row)
    {
        if(preg_match('/(.*?): (.*)/', $row, $matches)) {
            $headers[$matches[1]] = $matches[2];
        }
    }

    return $headers;
}


/**
 * Get body from multi cURL response
 *
 * @param $response
 * @return bool|string
 */
function asp_get_body_from_multi_curl_response($response)
{
    // Get the response headers as string
    return substr($response['content'], $response['header_size']);
}


/**
 * Get product details after perform multi curl
 *
 * @param $items
 * @return array
 */
function asp_get_result_product_details($items)
{
    $url = "https://api.aliseeks.com/v1/products"; //Realtime

    $options = get_option('asp_option_names');
    $api_key = (isset($options['asp_api_key']) && $options['asp_api_key']) ? $options['asp_api_key'] : '';

    $http_headers = array(
        'Content-Type: application/json',
        'X-Api-Client-Id: '.$api_key,
        'cache-control: no-cache'
    );

    $urls = array();
    foreach ($items as $key=>$val)
    {
        $urls[$key] = array(
            'url' => $url,
            'params' => array(
                "productId" => $items[$key]['id'],
                "components" => array("shipping")
            ),
            'http_headers' => $http_headers
        );
    }

    $result = asp_multi_curl_query($urls);
    $collection = array();
    $arr_headers = array();

    if(count($result))
    {
        foreach ($items as $key=>$val)
        {
            if(isset($result[$key]) && is_array($result[$key]))
            {
                $http_code = $result[$key]['http_code'];
                if($http_code == 200)
                {
                    $headers = asp_get_array_headers_from_multi_curl_response($result[$key]);
                    if(array_key_exists('x-remaining-requests', $headers)){
                        $arr_headers[] = intval( trim( $headers['x-remaining-requests'] ) );
                    }
                    $body = asp_get_body_from_multi_curl_response($result[$key]);
                    $json = @json_decode($body, true);

                    if(empty($json)){
                        $json = array();
                    }
                    $collection[] = array_merge($items[$key], array('details'=>$json)); // get all details!

//                    $freight = array();
//                    if(isset($json['shipping']) && !empty($json['shipping'])){
//
//                    }
//
//                    $details = array(
//                        'detailUrl' => $json['detailUrl'],
//                        'ratings'   => (isset($json['reviews']['ratings']) ? $json['reviews']['ratings'] : 0),
//                        'shipping'  => isset($json['shipping']) && !empty($json['shipping']) ? $json['shipping'][0] : 0
//                    );
//                    $collection[] = array_merge($items[$key], $details); // grt only detailUrl from details
                }
            }
        }

        if(count($collection) && count($arr_headers))
        {
            //update remaining requests realtime
            $options = get_option('asp_option_names');
            $remaining_requests = min($arr_headers);
            $options['asp_remaining_requests_realtime'] = $remaining_requests;
            update_option('asp_option_names', $options, 'no');
        }
    }

    $json = array('items'=>$collection);

    if(function_exists('cip_update_asp_items')){
        cip_update_asp_items($json);
    }

    $response = array_merge(array('ok'=> true), $json);

    return $response;
}


/**
 * AJAX - get result search by image Url
 */
function asp_get_result_search_by_image_url()
{
    $asp_active_plugin = (bool) get_option('asp_active_plugin');

    if( $asp_active_plugin )
    {
        $nonce = isset($_REQUEST['asp_nonce']) ? $_REQUEST['asp_nonce'] : '';

        if ( ! wp_verify_nonce( $nonce, 'asp_nonce' ) )
        {
            $response = array('ok'=>false, 'error'=>array('msg'=>'Failed nonce check!'));
        }
        else
        {
            if(!isset($_REQUEST['img_url']) || empty($_REQUEST['img_url']))
            {
                $response = array('ok'=>false, 'error'=>array('msg'=>'Bad request! Image URL not found!'));
            }
            else
            {
                $result = asp_curl_query_get_uploadKey_by_image_url($_REQUEST['img_url']);
                if($result['ok'] && $result['uploadKey'])
                {
                    $uploadKey = $result['uploadKey'];
                    $result = asp_curl_query_get_result_by_uploadKey($uploadKey);
                    if($result['ok'] && $result['items'])
                    {
                        $response = asp_get_result_product_details($result['items']);
                    }
                    else
                    {
                        $response = array('ok'=>false, 'error'=>array('msg'=>'No results!'));
                    }
                }
                else
                {
                    $response = array('ok'=>false, 'error'=>array('msg'=>'Bad request!'));
                }
            }
        }
    }
    else
    {
        $response = array('ok'=>false, 'error'=>array('msg'=>'Plugin not active!'));
    }

    echo json_encode($response);
    wp_die();
}
add_action('wp_ajax_asp_get_result_search_by_image_url', 'asp_get_result_search_by_image_url');
add_action('wp_ajax_nopriv_asp_get_result_search_by_image_url', 'asp_get_result_search_by_image_url');


/**
 * AJAX - get result search by image uploadKey
 */
function asp_get_result_search_by_image_upload_key()
{
    $asp_active_plugin = (bool) get_option('asp_active_plugin');

    if( $asp_active_plugin )
    {
        $nonce = isset($_REQUEST['asp_nonce']) ? $_REQUEST['asp_nonce'] : '';

        if ( ! wp_verify_nonce( $nonce, 'asp_nonce' ) )
        {
            $response = array('ok'=>false, 'error'=>array('msg'=>'Failed nonce check!'));
        }
        else
        {
            if(!isset($_REQUEST['upload_key']) || empty($_REQUEST['upload_key']))
            {
                $response = array('ok'=>false, 'error'=>array('msg'=>'Bad request! uploadKey not found!'));
            }
            else
            {
                $uploadKey = $_REQUEST['upload_key'];
                $result = asp_curl_query_get_result_by_uploadKey($uploadKey);
                if($result['ok'] && $result['items'])
                {
                    $response = asp_get_result_product_details($result['items']);
                }
                else
                {
                    $response = array('ok'=>false, 'error'=>array('msg'=>'No results!'));
                }
            }
        }
    }
    else
    {
        $response = array('ok'=>false, 'error'=>array('msg'=>'Plugin not active!'));
    }

    echo json_encode($response);
    wp_die();
}
add_action('wp_ajax_asp_get_result_search_by_image_upload_key', 'asp_get_result_search_by_image_upload_key');
add_action('wp_ajax_nopriv_asp_get_result_search_by_image_upload_key', 'asp_get_result_search_by_image_upload_key');



// -------------------------------------- Get product by product ID -------------------------------------------- //


/**
 * cUrl query - Get product by product ID
 *
 * @param $product_id
 * @return array
 */
function asp_get_product_by_id($product_id)
{
    $url = "https://api.aliseeks.com/v1/products"; //Realtime

    $options = get_option('asp_option_names');
    $api_key = (isset($options['asp_api_key']) && $options['asp_api_key']) ? $options['asp_api_key'] : '';

    $params = array(
        "productId" => $product_id
    );

    $http_headers = array(
        'Content-Type: application/json',
        'X-Api-Client-Id: '.$api_key,
        'cache-control: no-cache'
    );

    $result = asp_curl_query($url, $params, $http_headers);
    return $result;
}