<?php
/*
Plugin Name: AliExpress Searcher Plugin
Plugin URI: http://cookie.io/
Description: Search AliExpress products by keywords
Version: 1.0.5
Author: Web Design Sun Team
Author URI: https://www.webdesignsun.com/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Copyright: Web Design Sun Team
*/


define('ASP_PLUGIN_VERSION', '1.0.5');
define('ASP_MIXPANEL_PRODUCTION', true);
define('ASP_MAIN_FILE', __FILE__);//absolutely path to main plugin file
define('ASP_MAX_PAGING_PRODUCTS', 50);//max 50 - for normal work script
define('ASP_NORMAL_PAGING_PRODUCTS', 24);//24 - for normal work script

require_once('inc/init.php');//include plugin scripts + styles
require_once('inc/functions.php');//include plugin functions


/**
 * Activation plugin
 */
function asp_activation()
{
    update_option('asp_active_plugin', true, 'no');
}
register_activation_hook(__FILE__,'asp_activation');


/**
 * Deactivation plugin
 */
function asp_deactivation()
{
    update_option('asp_active_plugin', false, 'no');
}
register_deactivation_hook(__FILE__, 'asp_deactivation');


/**
 * Uninstall plugin
 */
function asp_uninstall()
{
    delete_option('asp_active_plugin');

    /**
     * asp_api_key       - https://www.aliseeks.com/accounts/overview
     * asp_pagination    - https://docs.aliseeks.com/api/#search-products
     */
    delete_option('asp_option_names');
}
register_uninstall_hook(__FILE__, 'asp_uninstall');


/**
 * Add the settings page to the menu.
 */
function asp_add_admin_options_menu()
{
    add_options_page(
        'AliExpress Settings Plugin', // Title
        'AliExpress Settings',        // Menu title
        'manage_options',             // Security
        'asp',                        // __FILE__ File to open
        'asp_options_page_output'     // Function to call
    );
}
add_action('admin_menu', 'asp_add_admin_options_menu');



/**
 * Register settings.
 */
function asp_plugin_settings()
{
    // params: $option_group, $option_name, $sanitize_callback
    register_setting( 'asp_option_group', 'asp_option_names', 'sanitize_callback' );

    // params: $id, $title, $callback, $page
    add_settings_section( 'asp_section_id', '', '', 'asp' );

    // params: $id, $title, $callback, $page, $section, $args
    add_settings_field('asp_api_key', 'App API Key', 'asp_fill_api_key', 'asp', 'asp_section_id' );
    add_settings_field('asp_pagination', 'App pagination', 'asp_fill_pagination', 'asp', 'asp_section_id' );
    add_settings_field('asp_remaining_requests', 'App remaining requests', 'asp_fill_remaining_requests', 'asp', 'asp_section_id' );
    add_settings_field('asp_remaining_requests_realtime', 'App remaining requests (realtime)', 'asp_fill_remaining_requests_realtime', 'asp', 'asp_section_id' );
    add_settings_field('asp_page_output', 'App page output', 'asp_fill_page_output', 'asp', 'asp_section_id' );
    add_settings_field('asp_page_search_by_image', 'Search by image on page', 'asp_fill_page_search_by_image', 'asp', 'asp_section_id' );
    add_settings_field('asp_product_reveal_page', __('Product reveal page'), 'asp_fill_product_reveal_page', 'asp', 'asp_section_id' );
    add_settings_field('asp_powered_by', 'App powered by', 'asp_fill_powered_by', 'asp', 'asp_section_id' );
}
add_action('admin_init', 'asp_plugin_settings');

function asp_fill_api_key()
{
    $val = get_option('asp_option_names');
    $val = ($val && isset($val['asp_api_key'])) ? $val['asp_api_key'] : null;
    ?>
    <input type="text" name="asp_option_names[asp_api_key]" value="<?php echo esc_attr( $val ) ?>" required/>
    <p class="description">Your API Access Key. <a href="https://www.aliseeks.com/accounts/overview" target="_blank">see</a></p>
    <?php
}

function asp_fill_pagination()
{
    $val = get_option('asp_option_names');
    $val = ($val && isset($val['asp_pagination'])) ? $val['asp_pagination'] : null;
    ?>
    <input type="number" name="asp_option_names[asp_pagination]" value="<?php echo esc_attr( $val ) ?>" step="1" min="1" max="<?php echo ASP_MAX_PAGING_PRODUCTS; ?>" placeholder="<?php echo ASP_NORMAL_PAGING_PRODUCTS; ?>"/>
    <p class="description"><?php _e('We recommend using no more than '.ASP_NORMAL_PAGING_PRODUCTS.' records.'); ?></p>
    <?php
}

function asp_fill_remaining_requests()
{
    $val = get_option('asp_option_names');
    $val = ($val && isset($val['asp_remaining_requests'])) ? $val['asp_remaining_requests'] : null;
    ?>
    <input type="text" name="asp_option_names[asp_remaining_requests]" value="<?php echo esc_attr( $val ) ?>" readonly/>
    <p class="description">The number of remaining requests in your subscription.</p>
    <?php
}

function asp_fill_remaining_requests_realtime()
{
    $val = get_option('asp_option_names');
    $val = ($val && isset($val['asp_remaining_requests_realtime'])) ? $val['asp_remaining_requests_realtime'] : null;
    ?>
    <input type="text" name="asp_option_names[asp_remaining_requests_realtime]" value="<?php echo esc_attr( $val ) ?>" readonly/>
    <p class="description">The number of remaining requests in your subscription (realtime).</p>
    <?php
}

function asp_fill_page_output()
{
    $val = get_option('asp_option_names');
    $val = ($val && isset($val['asp_page_output'])) ? $val['asp_page_output'] : null;

    $page_output_url = '';

    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1 //'posts_per_page' => -1 removes the limitation on the output pages
    );
    $query = new WP_Query($args);
    if ( $query->have_posts() )
    {
        ?>
        <select name="asp_option_names[asp_page_output]" required>
            <option value="">Select page</option>
            <?php
            while ($query->have_posts())
            {
                $query->the_post();
                $page_id = get_the_ID();
                $val = intval($val);

                if($val)
                {
                    $selected = ($page_id == $val) ? 'selected' : '';
                    $option = '<option value="' . get_the_ID() . '" '.$selected.'>' . get_the_title() . '</option>';
                    $page_output_url = get_permalink($val);
                }
                else
                {
                    $option = '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
                }

                echo $option;
            }
            wp_reset_postdata();
            ?>
        </select>
        <?php
    }

    if($page_output_url)
    {
        ?>
        <p class="description"><?php _e('Quick'); ?> <a href="<?php echo $page_output_url; ?>" target="_blank"><?php _e('view'); ?></a></p>
        <?php
    }
}

function asp_fill_page_search_by_image()
{
    $val = get_option('asp_option_names');
    $val = ($val && isset($val['asp_page_search_by_image'])) ? $val['asp_page_search_by_image'] : null;

    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1 //'posts_per_page' => -1 removes the limitation on the output pages
    );
    $query = new WP_Query($args);
    if ( $query->have_posts() )
    {
        ?>
        <select name="asp_option_names[asp_page_search_by_image]" required>
            <option value="">Select page</option>
            <?php
            while ($query->have_posts())
            {
                $query->the_post();
                $page_id = get_the_ID();
                $val = intval($val);

                if($val)
                {
                    $selected = ($page_id == $val) ? 'selected' : '';
                    $option = '<option value="' . get_the_ID() . '" '.$selected.'>' . get_the_title() . '</option>';
                }
                else
                {
                    $option = '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
                }

                echo $option;
            }
            wp_reset_postdata();
            ?>
        </select>
        <p class="description">Page which will perform search by image.</p>
        <?php
    }
}

function asp_fill_product_reveal_page()
{
    $val = get_option('asp_option_names');
    $val = ($val && isset($val['asp_product_reveal_page'])) ? $val['asp_product_reveal_page'] : null;

    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1
    );
    $query = new WP_Query($args);
    if ( $query->have_posts() )
    {
        ?>
        <select name="asp_option_names[asp_product_reveal_page]" required>
            <option value="">Select page</option>
            <?php
            while ($query->have_posts())
            {
                $query->the_post();
                $page_id = get_the_ID();
                $val = intval($val);

                if($val)
                {
                    $selected = ($page_id == $val) ? 'selected' : '';
                    $option = '<option value="' . get_the_ID() . '" '.$selected.'>' . get_the_title() . '</option>';
                }
                else
                {
                    $option = '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
                }

                echo $option;
            }
            wp_reset_postdata();
            ?>
        </select>
        <p class="description">Page which will perform search product reveal.</p>
        <?php
    }
}

function asp_fill_powered_by()
{
    ?>
    <p class="description">
        <a href="https://docs.aliseeks.com/api/" target="_blank">
            <img src="<?php echo plugins_url('/img/aliexpress-logo-api.png', ASP_MAIN_FILE); ?>" style="width: 125px;height: 25px;border: 1px solid #cccccc;">
        </a>
    </p>
    <?php
}



/**
 * Output settings page.
 */
function asp_options_page_output()
{
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }

    ?>
    <div class="wrap">
        <form action="options.php" method="POST">
            <?php
            settings_fields( 'asp_option_group' ); // hidden protective fields
            echo '<h1>'.esc_html( get_admin_page_title() ).'</h1>';
            do_settings_sections( 'asp' );         // sections with settings (options). We have only one section 'section_id'
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

