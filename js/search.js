/**
 * Search script
 */

jQuery(document).ready(function($)
{
    var debug = true;
    var process = false;
    var unit_prices = false;
    var objParams = '';
    var file_num_mb = 4;
    var file_size   = file_num_mb * 1024 * 1024;

    //path to file: /wp-admin/admin-ajax.php.
    var asp_path_ajax = AspLocalization.asp_path_ajax; //WP Localization js
    var counterSteps = 0;
    //var mainContainer = [];
    var indexerAliExpressBlocks = 0;

    var aliexpressListing = null;
    var defaultFreeShippingNum = 0;
    var defaultFreeShippingType = '';
    var defaultShippingNumFilter = 0;
    var objCallback = {
        countResult: 0,
        showResult: function () {},
        stopProcess: function () {}
    };

    var image_postfix1  = '.jpg_.webp';
    var image_postfix2  = '_.webp';
    var postfix_len1    = image_postfix1.length;
    var postfix_len2    = image_postfix2.length;


    var currencies_symbols = {
        'AED' : '&#x62f;.&#x625;',
        'AFN' : '&#x60b;',
        'ALL' : 'L',
        'AMD' : 'AMD',
        'ANG' : '&fnof;',
        'AOA' : 'Kz',
        'ARS' : '&#36;',
        'AUD' : '&#36;',
        'AWG' : 'Afl.',
        'AZN' : 'AZN',
        'BAM' : 'KM',
        'BBD' : '&#36;',
        'BDT' : '&#2547;&nbsp;',
        'BGN' : '&#1083;&#1074;.',
        'BHD' : '.&#x62f;.&#x628;',
        'BIF' : 'Fr',
        'BMD' : '&#36;',
        'BND' : '&#36;',
        'BOB' : 'Bs.',
        'BRL' : '&#82;&#36;',
        'BSD' : '&#36;',
        'BTC' : '&#3647;',
        'BTN' : 'Nu.',
        'BWP' : 'P',
        'BYR' : 'Br',
        'BYN' : 'Br',
        'BZD' : '&#36;',
        'CAD' : '&#36;',
        'CDF' : 'Fr',
        'CHF' : '&#67;&#72;&#70;',
        'CLP' : '&#36;',
        'CNY' : '&yen;',
        'COP' : '&#36;',
        'CRC' : '&#x20a1;',
        'CUC' : '&#36;',
        'CUP' : '&#36;',
        'CVE' : '&#36;',
        'CZK' : '&#75;&#269;',
        'DJF' : 'Fr',
        'DKK' : 'DKK',
        'DOP' : 'RD&#36;',
        'DZD' : '&#x62f;.&#x62c;',
        'EGP' : 'EGP',
        'ERN' : 'Nfk',
        'ETB' : 'Br',
        'EUR' : '&euro;',
        'FJD' : '&#36;',
        'FKP' : '&pound;',
        'GBP' : '&pound;',
        'GEL' : '&#x10da;',
        'GGP' : '&pound;',
        'GHS' : '&#x20b5;',
        'GIP' : '&pound;',
        'GMD' : 'D',
        'GNF' : 'Fr',
        'GTQ' : 'Q',
        'GYD' : '&#36;',
        'HKD' : '&#36;',
        'HNL' : 'L',
        'HRK' : 'Kn',
        'HTG' : 'G',
        'HUF' : '&#70;&#116;',
        'IDR' : 'Rp',
        'ILS' : '&#8362;',
        'IMP' : '&pound;',
        'INR' : '&#8377;',
        'IQD' : '&#x639;.&#x62f;',
        'IRR' : '&#xfdfc;',
        'IRT' : '&#x062A;&#x0648;&#x0645;&#x0627;&#x0646;',
        'ISK' : 'kr.',
        'JEP' : '&pound;',
        'JMD' : '&#36;',
        'JOD' : '&#x62f;.&#x627;',
        'JPY' : '&yen;',
        'KES' : 'KSh',
        'KGS' : '&#x441;&#x43e;&#x43c;',
        'KHR' : '&#x17db;',
        'KMF' : 'Fr',
        'KPW' : '&#x20a9;',
        'KRW' : '&#8361;',
        'KWD' : '&#x62f;.&#x643;',
        'KYD' : '&#36;',
        'KZT' : 'KZT',
        'LAK' : '&#8365;',
        'LBP' : '&#x644;.&#x644;',
        'LKR' : '&#xdbb;&#xdd4;',
        'LRD' : '&#36;',
        'LSL' : 'L',
        'LYD' : '&#x644;.&#x62f;',
        'MAD' : '&#x62f;.&#x645;.',
        'MDL' : 'MDL',
        'MGA' : 'Ar',
        'MKD' : '&#x434;&#x435;&#x43d;',
        'MMK' : 'Ks',
        'MNT' : '&#x20ae;',
        'MOP' : 'P',
        'MRO' : 'UM',
        'MUR' : '&#x20a8;',
        'MVR' : '.&#x783;',
        'MWK' : 'MK',
        'MXN' : '&#36;',
        'MYR' : '&#82;&#77;',
        'MZN' : 'MT',
        'NAD' : '&#36;',
        'NGN' : '&#8358;',
        'NIO' : 'C&#36;',
        'NOK' : '&#107;&#114;',
        'NPR' : '&#8360;',
        'NZD' : '&#36;',
        'OMR' : '&#x631;.&#x639;.',
        'PAB' : 'B/.',
        'PEN' : 'S/.',
        'PGK' : 'K',
        'PHP' : '&#8369;',
        'PKR' : '&#8360;',
        'PLN' : '&#122;&#322;',
        'PRB' : '&#x440;.',
        'PYG' : '&#8370;',
        'QAR' : '&#x631;.&#x642;',
        'RMB' : '&yen;',
        'RON' : 'lei',
        'RSD' : '&#x434;&#x438;&#x43d;.',
        'RUB' : '&#8381;',
        'RWF' : 'Fr',
        'SAR' : '&#x631;.&#x633;',
        'SBD' : '&#36;',
        'SCR' : '&#x20a8;',
        'SDG' : '&#x62c;.&#x633;.',
        'SEK' : '&#107;&#114;',
        'SGD' : '&#36;',
        'SHP' : '&pound;',
        'SLL' : 'Le',
        'SOS' : 'Sh',
        'SRD' : '&#36;',
        'SSP' : '&pound;',
        'STD' : 'Db',
        'SYP' : '&#x644;.&#x633;',
        'SZL' : 'L',
        'THB' : '&#3647;',
        'TJS' : '&#x405;&#x41c;',
        'TMT' : 'm',
        'TND' : '&#x62f;.&#x62a;',
        'TOP' : 'T&#36;',
        'TRY' : '&#8378;',
        'TTD' : '&#36;',
        'TWD' : '&#78;&#84;&#36;',
        'TZS' : 'Sh',
        'UAH' : '&#8372;',
        'UGX' : 'UGX',
        'USD' : '&#36;',
        'UYU' : '&#36;',
        'UZS' : 'UZS',
        'VEF' : 'Bs F',
        'VND' : '&#8363;',
        'VUV' : 'Vt',
        'WST' : 'T',
        'XAF' : 'CFA',
        'XCD' : '&#36;',
        'XOF' : 'CFA',
        'XPF' : 'Fr',
        'YER' : '&#xfdfc;',
        'ZAR' : '&#82;',
        'ZMW' : 'ZK',
    };


    if($('#aliexpress-item-filter-free').length > 0){
        defaultFreeShippingNum = (+ $('#aliexpress-item-filter-free').attr('data-filter') );
        defaultFreeShippingType = $('#aliexpress-item-filter-free').attr('title');
    }


    if($('#pageup').length > 0){
        $('#pageup').pageup();
    }


    if(window.AspLocalization.asp_production)
    {
        $('body').on('click', '.detail-link', function (e) {

            var detail_link = $(this).attr('data-detail-link');
            if(detail_link)
            {
                window.mixpanel.track( "Performed View on AliExpress", {
                    "$page_title": jQuery('.page-title').text(),
                    "$detail_link": detail_link
                });
            }

        });
    }


    if(window.AspLocalization.asp_action_change_cookied)
    {
        $('body').on('click', '.cookied-mark', function (e) {

            e.preventDefault();
            var $this = $(this);

            if(!$this.hasClass('cookied-mark')){
                return false;
            }

            $this.removeClass('cookied-mark');

            if($this.hasClass('icon-cookied')){
                var mark = true;
            }else{
                var mark = false;
            }

            changeIconCookiedMark($this, true, false);

            // var guid = $(this).attr('data-guid');
            // var json = mainContainer[guid];
            // if(typeof json === "object"){
            //     json['cookied'] = true;
            //     json = JSON.stringify(json);
            // }else{
            //     json = '';
            // }

            var json = get_cookied_object($this, (!mark));

            var params = {
                "action": window.AspLocalization.asp_action_change_cookied,
                "asp_nonce": window.AspLocalization.asp_nonce,
                "id": $(this).attr('data-id'),
                "type": window.AspLocalization.asp_type_cookied,
                "mark": +(!mark),
                "data": JSON.stringify(json)
            };

            var options = {
                url: window.AspLocalization.asp_path_ajax,
                data: getAdaptDebugParams( params ),
                method: 'POST',
                callback: function(data)
                {
                    if(data && data.ok)
                    {
                        changeIconCookiedMark($this, false, (!mark));
                    }
                    else
                    {
                        if(data.error && data.error.msg){
                            alert(data.error.msg);
                        }
                        changeIconCookiedMark($this, false, mark);
                    }
                    $this.addClass('cookied-mark');
                },
                callback_error: function (result)
                {
                    console.log('asp_action_change_cookied callback_error');
                    alert('Sorry, systems error!');
                    changeIconCookiedMark($this, false, mark);
                    $this.addClass('cookied-mark');
                }
            };

            send_ajax_options(options);

        });
    }


    function get_cookied_object(item, cookied)
    {
        var element = $(item).parents('.element-item');
        return {
            "id":                   element.attr('data-listing-id'),
            "post_img_url":         $('.image-block .post-image', element).attr('src'),
            "post_img_alt":         $('.image-block .post-image', element).attr('alt'),
            "detail_link":          $('.image-block a.detail-link', element).attr('href'),
            "search_by_image_link": $('.image-block a.search-by-image-link', element).attr('href'),
            "shopify_store_link":   $('.image-block a.shopify-store-link', element).attr('href'),
            "price":                $('.social-actions .price', element).text(),
            "price_currency":       $('.social-actions .price', element).attr('data-price-currency'),
            "ratings":              $('.social-actions.ratings', element).attr('data-ratings'),
            "orders":               $('.social-actions .orders', element).text(),
            "shipping_num":         $('.shipping', element).attr('data-shipping'),
            "shipping_type":        $('.shipping', element).attr('data-shipping-type'),
            "shipping_text":        $('.shipping', element).text(),
            "description":          $('.post-text-inner', element).text(),
            "cookied":              cookied
        };
    }


    if(window.AspLocalization.asp_production && window.AspLocalization.asp_search_by_image_page)
    {
        $('body').on('click', '.search-by-image-link', function (e) {

            var img_url = $(this).attr('data-img-url');
            if(img_url)
            {
                window.mixpanel.track( "Performed Compare Prices", {
                    "$page_title": jQuery('.page-title').text(),
                    "$img_url": img_url
                });
            }

        });
    }


    if(window.AspLocalization.asp_production && window.AspLocalization.asp_search_shopify_link)
    {
        $('body').on('click', '.shopify-store-link', function (e) {

            var img_url = $(this).attr('data-img-url');
            if(img_url)
            {
                window.mixpanel.track( "Performed Find Stores Selling This", {
                    "$page_title": jQuery('.page-title').text(),
                    "$img_url": img_url
                });
            }

        });
    }


    /**
     * Submit form
     */
    $('#aliexpress-search').on('submit', function(e)
    {
        if(process)
        {
            e.preventDefault();
            return false;
        }

        searchListing(true);

        e.preventDefault();
        return false;
    });


    /**
     * Click of link
     */
    jQuery('.aliexpress-load-more').click(function(e)
    {
        if(process)
        {
            e.preventDefault();
            return false;
        }

        searchListing(false);

        e.preventDefault();
        return false;
    });

    function getSearchParams()
    {
        unit_prices = $('#unit-prices').prop('checked');
        return {
            "keywords": $('#aliexpress-keywords').val(),
            //"sort": $('#select-search-by').val(),
            "price_min": $('#price-min').val(),
            "price_max": $('#price-max').val(),
            "quantity_min": $('#quantity-min').val(),
            "quantity_max": $('#quantity-max').val(),
            "rating_min": $('#rating-min').val(),
            "rating_max": $('#rating-max').val(),
            "order_min": $('#order-min').val(),
            "order_max": $('#order-max').val(),
            "unit_prices": unit_prices
        };
    }

    function setSearchParams(params)
    {
        var parseParams = JSON.parse(params);
        $('#aliexpress-keywords').val(parseParams.keywords);
        //$('#select-search-by').val(parseParams.sort);
        $('#price-min').val(parseParams.price_min);
        $('#price-max').val(parseParams.price_max);
        $('#quantity-min').val(parseParams.quantity_min);
        $('#quantity-max').val(parseParams.quantity_max);
        $('#rating-min').val(parseParams.rating_min);
        $('#rating-max').val(parseParams.rating_max);
        $('#order-min').val(parseParams.order_min);
        $('#order-max').val(parseParams.order_max);
        $('#unit-prices').prop('checked', parseParams.unit_prices);
        unit_prices = parseParams.unit_prices;
    }

    function sendMixPanelParams()
    {
        var performed_AliExpress_searches = window.mixpanel.get_property('$performed_AliExpress_searches');
        var total_searches_performed = window.mixpanel.get_property('$total_searches_performed');

        if(!performed_AliExpress_searches)
        {
            if(!total_searches_performed)
            {
                window.mixpanel.register({
                    "$performed_AliExpress_searches": 1,
                    "$total_searches_performed": 1
                });
            }
            else
            {
                total_searches_performed = parseInt(total_searches_performed);
                window.mixpanel.register({
                    "$performed_AliExpress_searches": 1,
                    "$total_searches_performed": (++total_searches_performed)
                });
            }
        }
        else
        {
            performed_AliExpress_searches = parseInt(performed_AliExpress_searches);
            performed_AliExpress_searches++;

            if(!total_searches_performed)
            {
                window.mixpanel.register({
                    "$performed_AliExpress_searches": performed_AliExpress_searches,
                    "$total_searches_performed": performed_AliExpress_searches
                });
            }
            else
            {
                total_searches_performed = parseInt(total_searches_performed);
                window.mixpanel.register({
                    "$performed_AliExpress_searches": performed_AliExpress_searches,
                    "$total_searches_performed": (++total_searches_performed)
                });
            }
        }

        //window.mixpanel.unregister('$performed_AliExpress_searches');

        unit_prices = $('#unit-prices').prop('checked');
        window.mixpanel.track( "Performed " + jQuery('.page-title').text(), {
            "$page_title":   jQuery('.page-title').text(),
            "$keywords":     $('#aliexpress-keywords').val(),
            "$price_min":    $('#price-min').val(),
            "$price_max":    $('#price-max').val(),
            "$quantity_min": $('#quantity-min').val(),
            "$quantity_max": $('#quantity-max').val(),
            "$rating_min":   $('#rating-min').val(),
            "$rating_max":   $('#rating-max').val(),
            "$order_min":    $('#order-min').val(),
            "$order_max":    $('#order-max').val(),
            "$unit_prices":  unit_prices
        });
    }

    /**
     * Main searching function
     *
     * if function running from event click of link, flagSubmit == false,
     * if function running from event submit form, flagSubmit == true
     *
     * @param flagSubmit
     */
    function searchListing(flagSubmit)
    {
        if($('#aliexpress-keywords').val() == '')
        {
            processSearchStop();
            hideTextMoreResult();
            resetCounterSteps();
            clearListing();
            showNoResults();
            hideErrors();
            hideResultsEnd();
            clearFileInput();
            //showDynamic();
        }
        else
        {
            var params = getSearchParams();

            /*//with changes
            if(JSON.stringify(params) !== objParams){
                flagSubmit = true;
                objParams = JSON.stringify(params);
            }*/

            //without changes
            if(JSON.stringify(params) !== objParams)
            {
                if(objParams === '')
                {
                    objParams = JSON.stringify(params);
                }
                else
                {
                    if(flagSubmit)
                    {
                        objParams = JSON.stringify(params);
                    }
                    else
                    {
                        setSearchParams(objParams);
                        params = getSearchParams();
                    }
                }
            }

            processSearchStart(flagSubmit);
            hideTextMoreResult();
            hideNoResults();
            hideErrors();
            hideResultsEnd();
            clearFileInput();

            if(flagSubmit)
            {
                resetCounterSteps();
                clearListing();
                hideFilterModule();
                //showDynamic();

                if(window.AspLocalization.asp_production){
                    sendMixPanelParams();
                }
            }

            jQuery.extend(params, { action: window.AspLocalization.asp_action_search, step: counterSteps });

            var options = {
                url: asp_path_ajax,
                data: getAdaptDebugParams(params),
                callback: function(data)
                {
                    if (data && data.ok)
                    {
                        if (data.aggregation.totalCount > 0)
                        {
                            var count_result = data.items.length + (+data.aggregation.skip);

                            $('.aliexpress-count-result').text(count_result);
                            objCallback.countResult = count_result;

                            if(data.aggregation.totalCount > count_result)
                            {
                                counterSteps++;
                                objCallback.showResult = showTextMoreResult;
                                objCallback.stopProcess = processSearchStop;
                            }
                            else
                            {
                                objCallback.showResult = showResultsEnd;
                                objCallback.stopProcess = processSearchStop;
                            }

                            if(data.items.length > 0)
                            {
                                initListing(data);
                            }
                        }
                        else
                        {
                            showNoResults();
                            processSearchStop();
                        }
                    }
                    else
                    {
                        clearListing();
                        if(data && data.error)
                        {
                            showErrors(data);
                        }
                        else
                        {
                            showNoResults();
                        }
                        processSearchStop();
                        hideFilterModule();
                    }
                },
                callback_error: function (result)
                {
                    processSearchStop();
                    console.log(result);
                }
            };

            send_ajax_options(options);
        }
    }


    /**
     * Function starts search process:
     * - Init process variable
     * - disabled Search button
     * - show preloader
     */
    function processSearchStart(flagSubmit)
    {
        process = true;
        $('.aliexpress-submit').prop('disabled', true);
        hideSearchByImageBtn();

        if(flagSubmit){
            $('.aliexpress-preloader-top').slideDown();
        }else{
            $('.aliexpress-preloader').slideDown();
        }

        changeIconBtnResetFilter(true);
    }

    /**
     * Function stops search process:
     * - reset process variable
     * - enabled Search button
     * - hide preloader
     */
    function processSearchStop()
    {
        process = false;
        $('.aliexpress-submit').prop('disabled', false);
        showSearchByImageBtn();
        $('.aliexpress-preloader').slideUp();

        changeIconBtnResetFilter(false);
    }


    //Functions showing and hiding elements


    /**
     * show dynamic elements
     * dynamic elements - work with mode (search)
     */
    function showDynamic()
    {
        $('.dynamic').fadeIn();
    }

    /**
     * hide dynamic elements
     * dynamic elements - don't work with mode (search by upload image), only with mode (search)
     */
    function hideDynamic()
    {
        $('.dynamic').fadeOut();
    }

    function showSearchByImageBtn()
    {
        $('.file-input-label').removeClass('disabled');
    }

    function hideSearchByImageBtn()
    {
        $('.file-input-label').addClass('disabled');
    }

    function showTextMoreResult()
    {
        $('.aliexpress-more-result').addClass('display-inline');
        $('.aliexpress-more-result').removeClass('display-none');
    }

    function hideTextMoreResult()
    {
        $(".aliexpress-more-result").addClass('display-none');
        $(".aliexpress-more-result").removeClass('display-inline');
    }

    function showNoResults()
    {
        $('.aliexpress-no-results').fadeIn();
    }

    function hideNoResults()
    {
        $('.aliexpress-no-results').fadeOut();
    }

    function clearListing()
    {
        if(aliexpressListing !== null){
            aliexpressListing.isotope( 'remove', aliexpressListing.isotope('getItemElements'));
            $('#aliexpress-listing').css('height', 0);
        }

        objCallback.countResult = 0;

        //mainContainer = [];
        resetCounterAliExpressBlocks();
    }

    function showErrors( data )
    {
        $('.aliexpress-results-error').text(data.error.msg).fadeIn();
    }

    function hideErrors()
    {
        $('.aliexpress-results-error').text('').fadeOut();
    }

    function showResultsEnd()
    {
        $('.aliexpress-results-end').fadeIn();
    }

    function hideResultsEnd()
    {
        $('.aliexpress-results-end').fadeOut();
    }

    function showResetFilter()
    {
        $('.aliexpress-reset-filter').fadeIn();
    }

    function hideResetFilter()
    {
        $('.aliexpress-reset-filter').fadeOut();
    }

    function showFilterModule()
    {
        $('.aliexpress-filters').css('opacity', 1);
    }

    function hideFilterModule()
    {
        $('.aliexpress-filters').css('opacity', 0);
    }

    function resetSorter()
    {
        var dropdownSort = $('#dropdownMenuSortBy');
        dropdownSort.text(dropdownSort.attr('data-default-text'));
        var value = dropdownSort.attr('data-default-sort');
        dropdownSort.attr('data-sort', value);
        $('.aliexpress-sort').removeClass('active');
        $('.aliexpress-sort[data-sort="'+value+'"]').addClass('active');
    }

    function resetShipFilter()
    {
        var dropdownShipFilter = $('#dropdownMenuShipFilterBy');
        dropdownShipFilter.text(dropdownShipFilter.attr('data-default-text'));
        var value = dropdownShipFilter.attr('data-default-filter');
        dropdownShipFilter.attr('data-filter', value);
        $('.aliexpress-ship-filter').removeClass('active');
        $('.aliexpress-ship-filter[data-filter="'+value+'"]').addClass('active');
    }

    /**
     * Reset counter steps
     */
    function resetCounterSteps()
    {
        counterSteps = 0;
    }

    /**
     * Reset counter AliExpress blocks
     */
    function resetCounterAliExpressBlocks()
    {
        indexerAliExpressBlocks = 0;
    }


    /**
     * Initialization params listing
     *
     * @param params
     */
    function initParamsListing(params)
    {
        var sortAscending = false;

        if(params.hasOwnProperty('sortBy'))
        {
            if(params.sortBy === 'best' || params.sortBy === 'ordersAsc' || params.sortBy === 'priceAsc'){
                sortAscending = true;
            }
        }

        var options = {
            itemSelector: '.element-item',
            layoutMode: 'masonry', /*masonry || fitRows*/
            resizable: true,
            /*transitionDuration: 0,*/
            getSortData: {
                /*lotSize: function(elem) {
                    var res = $(elem).find('.lot-size').text();
                    return parseInt(res, 10) || 0;
                },*/
                /*likes*/
                /*positiveFeedback: function(elem) {
                    var res = $(elem).find('.positive-feedback').attr('data-feedback');
                    return parseInt(res, 10) || 0;
                },
                feedback: function(elem) {
                    var res = $(elem).find('.feedback').attr('data-feedback');
                    return parseInt(res, 10) || 0;
                },
                defaultSort: function(elem) {
                    var res = $(elem).find('.default').text();
                    return parseInt(res, 10) || 0;
                }*/
                ratings: function(elem) {
                    var res = $(elem).find('.ratings').attr('data-ratings');
                    return parseFloat(res) || 0;
                },
                ordersAsc: function(elem) {
                    var res = $(elem).find('.orders').text();
                    return parseInt(res, 10) || 0;
                },
                ordersDesc: function(elem) {
                    var res = $(elem).find('.orders').text();
                    return parseInt(res, 10) || 0;
                },
                priceAsc: function(elem) {
                    var res = $(elem).find('.price').text();
                    return parseFloat(res) || 0;
                },
                priceDesc: function(elem) {
                    var res = $(elem).find('.price').text();
                    return parseFloat(res) || 0;
                },
                best: function(elem) {
                    var res = $(elem).find('.best').text();
                    return parseInt(res, 10) || 0;
                }
            },
            sortBy: 'best', /*functions name*/
            filter: function() {
                if(defaultShippingNumFilter === 0){
                    return true;
                }else{
                    var shipping = (+ $(this).find('.shipping').attr('data-shipping') );
                    return (shipping === defaultShippingNumFilter);
                }
            },
            sortAscending: sortAscending
        };

        jQuery.extend(options, params);

        if(aliexpressListing === null)
        {
            aliexpressListing = $('#aliexpress-listing').isotope(options);
            aliexpressListing.on('layoutComplete', function (event, laidOutItems) {

                setTimeout(function () {
                    eventLayoutComplete(event, laidOutItems);
                }, 1000);

            });
        }
        else
        {
            aliexpressListing.isotope(options);
        }
    }

    /**
     * Event - Triggered after a layout and all positioning transitions have completed.
     *
     * @param event
     * @param laidOutItems
     */
    function eventLayoutComplete(event, laidOutItems)
    {
        var children_length = $('#aliexpress-listing').children().length;
        var isotope_children_length = aliexpressListing.isotope('getItemElements').length;
        var isotope_filtered_length = aliexpressListing.isotope('getFilteredItemElements').length;

        if(children_length > 0 && children_length == objCallback.countResult)
        {
            if(debug)
            {
                // console.log( 'Isotope layout completed on ' + laidOutItems.length + ' items' );
                console.log( 'all ' + isotope_children_length + ' items' );
                console.log( 'display ' + isotope_filtered_length + ' items' );
                console.log( '--------------------------------' );
            }

            $('#total-filter-result').text( isotope_children_length );
            $('#total-more-result').text( isotope_children_length );
            $('#current-show-filter-result').text( isotope_filtered_length );
            $('#current-show-more-result').text( isotope_filtered_length );

            objCallback.showResult();
            showFilterModule();
            objCallback.stopProcess();

            //display tooltip !
            //jQuery('[data-toggle="tooltip"]').tooltip();
        }
        else
        {
            setTimeout(function () {
                if(debug){
                    console.log( 'repeat eventLayoutComplete' );
                }
                eventLayoutComplete(event, laidOutItems);
            }, 1000);
        }
    }


    /**
     * Get new element by once item from API
     *
     * @param item   - once item from API response
     * @param guid   - globally unique identifiers for sort by Best Match
     * @param object - flag TRUE or FALSE
     * @returns {*}  - if object == TRUE get jQuery object, else get string
     */
    function getNewListingItem(item, guid, object)
    {
        var image_url        = '';
        var image            = '';
        var img_src          = '';
        var description      = '';
        var shipping_price   = '';
        var shipping_type    = '';
        var shipping_out     = '';
        var shipping_num     = defaultFreeShippingNum;
        var shipping_class   = 'free-shipping';
        var ratings          = 0;
        var ratings_title    = '';
        var currency_symbol  = '';
        /*var totalFeedback    = 0;
        var positiveFeedback = 0;*/
        var lot              = 0;
        var lotUnit          = '';
        var lot_html_block   = '';
        var lot_unit_html    = '';
        var price            = 0;
        var detail_block     = '';
        var detail_link      = '';
        var cookied_block    = '';
        var search_shopify_link   = '';
        var search_shopify_block  = '';
        var search_by_image_link  = '';
        var search_by_image_block = '';

        //image
        try{
            //for example:
            //https://ae01.alicdn.com/kf/HTB1lDbVb3LD8KJjSszeq6yGRpXay.jpg_960x960.jpg_.webp
            //if exist postfix (.jpg_.webp), remove only postfix (_.webp)
            //get https://ae01.alicdn.com/kf/HTB1lDbVb3LD8KJjSszeq6yGRpXay.jpg_960x960.jpg
            //else not change!

            image_url = item.imageUrl;
            if(image_url.length)
            {
                var ts = image_url.slice((image_url.length - postfix_len1), image_url.length);
                if(ts === image_postfix1){
                    image_url = image_url.slice(0, (image_url.length - postfix_len2));
                }
            }
        }catch (exception){
            image_url = '';
        }

        try{
            detail_block = '<a class="btn detail-link" href="'+ item.detailUrl +'" target="_blank" data-detail-link="'+item.detailUrl+'">View on AliExpress</a>';
            detail_link = item.detailUrl;
        }
        catch (exception){
            detail_block = '';
            detail_link = '';
        }

        if(AspLocalization.asp_search_shopify_link && image_url)
        {
            //internal-link
            search_by_image_link = AspLocalization.asp_search_by_image_page + '?img_url=' + encodeURIComponent(image_url);
            search_by_image_block = '<a class="btn search-by-image-link" href="'+search_by_image_link+'" target="_blank" data-img-url="'+image_url+'">Compare Prices</a>';

            //external-link
            search_shopify_link = AspLocalization.asp_search_shopify_link + '?img_url=' + encodeURIComponent(image_url);
            search_shopify_block = '<a class="btn shopify-store-link" href="'+search_shopify_link+'" target="_blank" data-img-url="'+image_url+'">Find Stores Selling This</a>';
        }

        if(AspLocalization.asp_action_change_cookied)
        {
            cookied_block = "<div class='col-2 align-self-start cookied-container'>" +
                    "<i class='fa icon-cookied"+(item.cookied ? "" : "-o")+" cookied-mark' aria-hidden='true' data-id='"+item.id+"' data-guid='"+guid+"'></i>" +
                "</div>";
        }

        try{
            if(image_url){
                image = "<img src='"+ image_url +"' alt='"+ item.title +"' class='post-image' data-listing-id='"+item.id+"'>";
                img_src = image_url;
            }else{
                image = "<img src='"+ AspLocalization.asp_url_no_img +"' alt='"+ item.title +"' class='post-image' data-listing-id='"+item.id+"'>";
                img_src = AspLocalization.asp_url_no_img;
            }
        }catch (exception){
            image = "<img src='"+ AspLocalization.asp_url_no_img +"' alt='"+ item.title +"' class='post-image' data-listing-id='"+item.id+"'>";
            img_src = AspLocalization.asp_url_no_img;
        }

        //lot
        try{
            lot = parseInt(item.lotSize) || 0;

            if(lot > 1)
            {
                lotUnit = item.lotUnit || '';
                lot_html_block = '<div class="row ml-0 mr-0">'
                        +'<div class="col">'
                            +'<span class="social-actions lot-container">'
                                +'<span class="lot-unit">'+ (lot + " " + lotUnit + " / lot") +'</span>'
                                +'<span class="lot-size" style="display: none;">'+ lot +'</span>'
                            +'</span>'
                        +'</div>'
                    +'</div>';
            }

        }catch(exception){
            lot = 0;
            lotUnit = '';
            lot_html_block = '';
        }

        //price
        try{
            price = (Math.round(item.price.value * 100) / 100).toFixed(2);
            if(unit_prices && lot > 1)
            {
                price = (price / lot).toFixed(2);
                lot_unit_html = '<span class="lot-unit">'+ (" / " + lotUnit) +'</span>';
            }
        }catch(exception){
            price = 0;
        }

        //description
        try{
            description = item.title ? ((item.title).substring(0, 200) + '...' ) : '';
        }catch (exception){
            description = '';
        }

        //currency symbol
        try{
            currency_symbol = getCurrencySymbol( item.price.currency );
        }catch (exception){
            currency_symbol = '';
        }

        //shipping
        try{
            shipping_price = parseFloat(item.freight.price.value) || 0;
        }catch(exception){
            shipping_price = 0;
        }
        
        try{
            shipping_type = item.freight.type;
            if(shipping_price)
            {
                shipping_num = getShippingNumByType(shipping_type);
                if(unit_prices && lot > 1)
                {
                    shipping_price = shipping_price / lot;
                    shipping_price = shipping_price > 0 ? (shipping_price).toFixed(2) : shipping_price;
                }
                shipping_out = 'Shipping: '+currency_symbol+shipping_price+' via '+shipping_type;
                shipping_class = '';
            }
            else
            {
                shipping_out = defaultFreeShippingType;
            }
        }catch(exception){
            shipping_out = defaultFreeShippingType;
            console.log('Shipping type exception!');
            console.log(exception);
        }

        //ratings
        try{
            ratings = parseFloat(item.ratings) || 0;
            ratings_title = 'title="'+ ratings +' Ratings"';
        }catch(exception){
            ratings = 0;
            ratings_title = '';
        }

        var tmp = '<div class="col-md-4 col-sm-6 col-xs-6 col-xl-3 element-item" data-listing-id="'+item.id+'" data-guid="'+guid+'">'
            +'<div class="box">'
                +'<div class="row">'
                    +'<div class="col">'
                        +'<div class="image-block">'
                            +'<div class="bg-wrap" style="background-image: url('+ img_src +');">'
                                +image
                            +'</div>'
                            +'<div class="button-block row align-items-center no-gutters">'
                                +'<div class="col-12">'
                                    +detail_block
                                    +search_by_image_block
                                    +search_shopify_block
                                +'</div>'
                            +'</div>'
                        +'</div>'
                    +'</div>'
                +'</div>'
                +'<div class="row ml-0 mr-0 line-3 bt-1 align-items-center no-gutters">'
                    +'<div class="col-4">'
                        /* Price */
                        +'<span class="social-actions line price-container">'
                            +'<i class="fa fa-usd" aria-hidden="true"></i>'
                            +'<span class="price" data-price-currency="'+currency_symbol+'">'+ price +'</span>'
                            /*+ lot_unit_html*/
                        +'</span>'
                        /* Best Match */
                        +'<span class="best">'+ guid +'</span>'
                    +'</div>'
                    +'<div class="col-5">'
                        /* Rating */
                        +'<span class="social-actions ratings" data-toggle="tooltip" data-placement="bottom" data-ratings="'+ ratings +'" '+ ratings_title +'>'
                            + getRatingStarsHtml(ratings)
                        +'</span>'
                    +'</div>'
                    +'<div class="col-3">'
                        /* Orders */
                        +'<span class="social-actions orders-container" data-toggle="tooltip" data-placement="bottom" title="'+ item.orders +' Orders">'
                            +'<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shopping-bag" class="svg-inline--fa fa-shopping-bag fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20">'
                                +'<path fill="currentColor" d="M352 160v-32C352 57.42 294.579 0 224 0 153.42 0 96 57.42 96 128v32H0v272c0 44.183 35.817 80 80 80h288c44.183 0 80-35.817 80-80V160h-96zm-192-32c0-35.29 28.71-64 64-64s64 28.71 64 64v32H160v-32zm160 120c-13.255 0-24-10.745-24-24s10.745-24 24-24 24 10.745 24 24-10.745 24-24 24zm-192 0c-13.255 0-24-10.745-24-24s10.745-24 24-24 24 10.745 24 24-10.745 24-24 24z"></path>'
                            +'</svg>'
                            +'<span class="orders">'+ item.orders +'</span>'
                        +'</span>'
                    +'</div>'
                +'</div>'
                +'<div class="row ml-0 mr-0 line-3 bt-1 dis-flex align-content-center">'
                    +'<div class="col align-self-center">'
                        +'<div class="post-text">'
                            +'<span class="shipping '+shipping_class+'" data-shipping="'+ shipping_num +'" data-shipping-type="'+ shipping_type +'">'+ shipping_out +'</span>'
                        +'</div>'
                    +'</div>'
                +'</div>'
                +'<div class="row ml-0 mr-0 line-6 bt-1 dis-flex align-content-center">'
                    +'<div class="col-'+(cookied_block ? '10' : '12')+' align-self-center">'
                        +'<div class="post-text">'
                            +'<a href="'+detail_link+'" target="_blank" class="post-text-inner">'+ description +'</a>'
                        +'</div>'
                    +'</div>'
                    +cookied_block
                +'</div>'
            +'</div>'
        +'</div>';

        return object ? $(tmp) : tmp;
    }

    /**
     * Get rating stars HTML
     *
     * 4 stars for 4.0, 4.1, 4.2
     * 4,5 stars for 4.3, 4.4, 4.5, 4.6, 4.7
     * 5 stars for 4.8, 4.9, 5.0
     *
     * @param rating
     * @returns {string}
     */
    function getRatingStarsHtml(rating)
    {
        var max_stars = 5;
        var stars = '';

        if(rating)
        {
            var min_rating = Math.floor(rating);
            var remainder = Math.round((rating - min_rating) * 10) / 10; // rounding to decimal. If need rounding to hundredths - (n*100)/100
            var remainder_rating = Math.floor(max_stars - rating);

            if(min_rating)
            {
                //full star
                for(var i=0; i<min_rating; i++)
                {
                    stars += '<i class="fa fa-star" aria-hidden="true"></i>';
                }
            }

            if(remainder)
            {
                if(remainder <= 0.2){
                    stars += '<i class="fa fa-star-o" aria-hidden="true"></i></i>';
                } else if(remainder > 0.2 && remainder <= 0.7){
                    stars += '<i class="fa fa-star-half-o" aria-hidden="true"></i>\n';
                } else {
                    stars += '<i class="fa fa-star" aria-hidden="true"></i></i>';
                }
            }

            if(remainder_rating)
            {
                //empty star
                for(var z=0; z<remainder_rating; z++)
                {
                    stars += '<i class="fa fa-star-o" aria-hidden="true"></i>';
                }
            }
        }
        else
        {
            //empty star
            for(var y=0; y<max_stars; y++)
            {
                stars += '<i class="fa fa-star-o" aria-hidden="true"></i>';
            }
        }

        return stars;
    }


    /**
     * Generate and initialization listing
     *
     * @param data
     */
    function initListing(data)
    {
        initShippingNumFilter();

        var params = {
            sortBy: $('.aliexpress-sort.active').attr('data-sort')
        };

        initParamsListing(params);

        var blocks = '';
        $.each(data.items, function(i, item)
        {
            //mainContainer[indexerAliExpressBlocks] = item;
            blocks += getNewListingItem(item, indexerAliExpressBlocks, false);
            indexerAliExpressBlocks++;
        });

        blocks = $(blocks);
        loadListing(blocks);
    }

    /**
     * Loading Listing
     *
     * Use library for load images
     * https://imagesloaded.desandro.com/
     *
     * @param blocks
     */
    function loadListing(blocks)
    {
        blocks.imagesLoaded().progress( function( instance, image ) {

            if(debug){
                var result = image.isLoaded ? ' loaded.' : ' not loaded.';
                console.log( 'Image ' + image.img.src + result );
            }

            if(!image.isLoaded){
                image.img.src = AspLocalization.asp_url_no_img;
            }

        }).fail( function( instance ) {

            var broken = 0;
            for ( var i = 0, len = instance.images.length; i < len; i++ ) {
                var image = instance.images[i];
                if(!image.isLoaded){
                    broken++;
                }
            }

            if(debug){
                console.log('FAIL images: ' + broken);
            }

        }).always( function( instance ) {

            aliexpressListing.append( blocks );
            aliexpressListing.isotope( 'insert', blocks );

        });
    }

    $('.aliexpress-sort').click(function (e) {

        var _this = $(this);
        var sort = _this.attr('data-sort');

        var active_sort = $('#dropdownMenuSortBy').attr('data-sort');

        if(active_sort != sort)
        {
            changeIconBtnResetFilter(true);
            showResetFilter();

            $('#dropdownMenuSortBy').text(_this.text());
            $('#dropdownMenuSortBy').attr('data-sort', sort);

            $('.aliexpress-sort').removeClass('active');
            _this.addClass('active');

            var params = {
                sortBy: sort
            };

            initShippingNumFilter();
            initParamsListing(params);
            setTimeout(function () {
                changeIconBtnResetFilter(false);
            }, 300);
        }

        e.preventDefault();
    });


    $('.aliexpress-ship-filter').click(function (e) {

        var _this = $(this);
        var filter = (+ _this.attr('data-filter'));

        var active_filter = $('#dropdownMenuShipFilterBy').attr('data-filter');

        if(active_filter != filter)
        {
            changeIconBtnResetFilter(true);
            showResetFilter();

            $('#dropdownMenuShipFilterBy').text(_this.text());
            $('#dropdownMenuShipFilterBy').attr('data-filter', filter);

            $('.aliexpress-ship-filter').removeClass('active');
            _this.addClass('active');

            var params = {
                sortBy: $('.aliexpress-sort.active').attr('data-sort')
            };

            initShippingNumFilter();
            initParamsListing(params);
            setTimeout(function () {
                changeIconBtnResetFilter(false);
            }, 300);
        }

        e.preventDefault();
    });


    $('.aliexpress-reset-filter-btn').click(function (e) {

        hideResetFilter();

        resetSorter();
        resetShipFilter();

        var params = {
            sortBy: $('.aliexpress-sort.active').attr('data-sort')
        };

        initShippingNumFilter();
        initParamsListing(params);
    });


    function changeIconBtnResetFilter(flag)
    {
        if(flag){
            $('.aliexpress-reset-filter-btn>i').removeClass('fa-ban');
            $('.aliexpress-reset-filter-btn>i').addClass('fa-spinner fa-spin');
        } else {
            $('.aliexpress-reset-filter-btn>i').removeClass('fa-spinner fa-spin');
            $('.aliexpress-reset-filter-btn>i').addClass('fa-ban');
        }
    }


    function changeIconCookiedMark(element, flag, mark)
    {
        if(flag){
            $(element).removeClass('icon-cookied-o').removeClass('icon-cookied');
            $(element).addClass('fa-spinner fa-spin');
        } else {
            $(element).removeClass('fa-spinner fa-spin');
            if(mark){
                $(element).addClass('icon-cookied');
            }else{
                $(element).addClass('icon-cookied-o');
            }
        }
    }


    /**
     * Get shipping number by type
     *
     * @param type
     * @returns {number}
     */
    function getShippingNumByType(type)
    {
        switch (type)
        {
            case 'AliExpress Standard Shipping':{
                return 1;
            }
            case 'China Post Ordinary Small Packet Plus':{
                return 2;
            }
            case 'ePacket':{
                return 3;
            }
            case 'DHL':{
                return 4;
            }
            case 'EMS':{
                return 5;
            }
            case 'Yanwen Economic Air Mail':{
                return 6;
            }
            /*7 - Free Shipping*/
            default:{
                return 0; //Any Type
            }
        }
    }


    /**
     * Init shipping number from filter
     */
    function initShippingNumFilter()
    {
        defaultShippingNumFilter = (+ $('.aliexpress-ship-filter.active').attr('data-filter'));
    }


    /**
     * Get currency symbol
     *
     * @param currency
     * @returns {*}
     */
    function getCurrencySymbol( currency )
    {
        if(currencies_symbols.hasOwnProperty(currency)){
            return currencies_symbols[currency];
        }else{
            return '';
        }
    }


    /**
     * Return format feedback string
     * with or without (K)
     *
     * @param feedbacks
     * @returns {*}

    function formatFeedBack(feedbacks)
    {
        if(feedbacks){
            var f = feedbacks/1000;
            if(f >= 1){
                return Math.round(f) + 'K';
            }else{
                return feedbacks;
            }
        }else{
            return feedbacks;
        }
    }*/


    /**
     * Adapting params, if debug == true, addition XDEBUG params to request
     *
     * @param params
     * @returns {*}
     */
    function getAdaptDebugParams(params)
    {
        if(debug)
        {
            var options = {
                XDEBUG_SESSION_START: 'PHPSTORM'
            };
            jQuery.extend(params, options);
        }

        return params;
    }

    /**
     * Function send ajax request
     *
     * @param params
     */
    function send_ajax_options(params)
    {
        var options = {
            callback: function () {},
            callback_error: function () {},
            method: 'GET'
        };

        jQuery.extend(options, params);

        jQuery.ajax({
            url:  options.url,
            data: options.data,
            type: options.method,
            dataType: 'json',
            success: function (result, textStatus, jqXHR) {
                if(typeof options.callback == 'function'){
                    options.callback(result);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('function send_ajax_options error!');
                if(typeof options.callback_error == 'function'){
                    options.callback_error(jqXHR);
                }
            }
        })
    }


    // ---------------------------------------- Search By Image Upload ----------------------------------------- //


    /**
     * Note! Need use this variation because File Input (innerHTML) will be rewrite!
     * And EventListener will not be work!
     */
    $('body').on('change', '#file-input',function (e) {

        if($(this).val()){
            //run search by image
            if(process)
            {
                e.preventDefault();
                return false;
            }

            runAjaxGetAliExpressSearchByImageUpload();
        }

    });


    /**
     * Run AJAX query - Get AliExpress Search By Image Upload
     */
    function runAjaxGetAliExpressSearchByImageUpload()
    {
        var files = $('#file-input');
        if(files[0].files.length > 0)
        {
            processSearchStart(true);
            hideTextMoreResult();
            hideNoResults();
            hideErrors();
            hideResultsEnd();

            resetCounterSteps();
            clearListing();
            hideFilterModule();
            //hideDynamic();

            var file = files[0].files[0];
            if ( file.type.match('image/jpeg') || file.type.match('image/jpg') || file.type.match('image/png') )
            {
                if(file.size < file_size)
                {
                    var formData = new FormData();
                    formData.append('file', file);

                    var header = {};
                    header[window.AspLocalization.asp_api_param_name] = window.AspLocalization.asp_api_key;

                    var options = {
                        headers: header,
                        url: window.AspLocalization.asp_upload_url,
                        data: formData,
                        method: 'POST',
                        callback: function(data)
                        {
                            if(data && data.uploadKey){
                                console.log(data.uploadKey);
                                if(window.AspLocalization.asp_production)
                                {
                                    window.mixpanel.track( "Performed Search by upload image", {
                                        "$page_title": jQuery('.page-title').text(),
                                        "$upload_key": data.uploadKey
                                    });
                                }
                                runAjaxGetAliExpressSearchByImageUploadKey(data.uploadKey);
                            }else{
                                console.log(data);
                                showNoResults();
                                processSearchStop();
                            }
                        },
                        callback_error: function (result)
                        {
                            console.log('runAjaxGetAliExpressSearchByImageUpload callback_error');
                            console.log(result);
                            showNoResults();
                            processSearchStop();
                        }
                    };

                    send_ajax_multipart(options);
                }
                else
                {
                    clearFileInput();
                    showErrors({error: {msg:'File must be less than '+file_num_mb+'MB!'}});
                    processSearchStop();
                }
            }
            else
            {
                clearFileInput();
                showErrors({error: {msg:'File must have .jpg|.jpeg|.png extension!'}});
                processSearchStop();
            }
        }
    }

    /**
     * Run AJAX query - Get AliExpress Search By Image uploadKey
     */
    function runAjaxGetAliExpressSearchByImageUploadKey(uploadKey)
    {
        var params = {
            "action": window.AspLocalization.asp_action_search_by_image_upload_key,
            "asp_nonce": window.AspLocalization.asp_nonce,
            "upload_key": uploadKey
        };

        var options = {
            url: window.AspLocalization.asp_path_ajax,
            data: getAdaptDebugParams( params ),
            method: 'POST',
            callback: function(data)
            {
                if(data && data.ok)
                {
                    if(data.items.length > 0)
                    {
                        var count_result = data.items.length ;

                        $('.aliexpress-count-result').text(count_result);
                        objCallback.countResult = count_result;
                        objCallback.showResult = function (){};
                        objCallback.stopProcess = processSearchStop;

                        initListingSearchByImageUrl(data);
                    }
                    else
                    {
                        showNoResults();
                        processSearchStop();
                    }
                }
                else
                {
                    showNoResults();
                    processSearchStop();
                    console.log(data.error.msg);
                }
            },
            callback_error: function (result)
            {
                console.log('runAjaxGetAliExpressSearchByImageUploadKey callback_error');
                console.log(result);
                showNoResults();
                processSearchStop();
            }
        };

        send_ajax_options(options);
    }

    /**
     * Generate and initialization listing
     *
     * @param data
     */
    function initListingSearchByImageUrl(data)
    {
        initShippingNumFilter();

        var params = {
            sortBy: $('.aliexpress-sort.active').attr('data-sort')
        };

        initParamsListing(params);

        var blocks = '';
        $.each(data.items, function(i, item)
        {
            //mainContainer[indexerAliExpressBlocks] = item;
            blocks += getNewListingItemSearchByImageUrl(item, indexerAliExpressBlocks, false);
            indexerAliExpressBlocks++;
        });

        blocks = $(blocks);
        loadListing(blocks);
    }

    /**
     * Get new element by once item from API
     *
     * @param item   - once item from API response
     * @param guid   - globally unique identifiers for sort by Best Match
     * @param object - flag TRUE or FALSE
     * @returns {*}  - if object == TRUE get jQuery object, else get string
     */
    function getNewListingItemSearchByImageUrl(item, guid, object)
    {
        var image_url             = '';
        var image                 = '';
        var description           = '';
        var price                 = 0;
        var detail_link           = '';
        var detail_block          = '';
        var search_shopify_link   = '';
        var search_shopify_block  = '';
        var search_by_image_link  = '';
        var search_by_image_block = '';
        var img_src = '';

        var shipping         = '';
        var shipping_price   = '';
        var shipping_type    = '';
        var shipping_out     = '';
        var shipping_num     = defaultFreeShippingNum;
        var shipping_class   = 'free-shipping';
        var ratings          = 0;
        var ratings_title    = '';
        var currency_symbol  = '';
        /*var totalFeedback    = 0;
        var positiveFeedback = 0;*/
        var lot              = 0;
        var lotUnit          = '';
        var lot_html_block   = '';
        var lot_unit_html    = '';
        var cookied_block    = '';

        try{
            detail_link = item.detailUrl ? item.detailUrl : ((item.details && item.details.detailUrl) ? item.details.detailUrl : '');
        }catch (exception){
            detail_link = '';
        }

        //image
        try{
            //for example:
            //https://ae01.alicdn.com/kf/HTB1lDbVb3LD8KJjSszeq6yGRpXay.jpg_960x960.jpg_.webp
            //if exist postfix (.jpg_.webp), remove only postfix (_.webp)
            //get https://ae01.alicdn.com/kf/HTB1lDbVb3LD8KJjSszeq6yGRpXay.jpg_960x960.jpg
            //else not change!

            image_url = item.imageUrl;
            if(image_url.length)
            {
                var ts = image_url.slice((image_url.length - postfix_len1), image_url.length);
                if(ts === image_postfix1){
                    image_url = image_url.slice(0, (image_url.length - postfix_len2));
                }
            }
        }catch (exception){
            image_url = '';
        }

        try{
            if(detail_link)
            {
                detail_block = '<div class="detail-block">' +
                        '<a class="detail-link btn" href="'+ detail_link +'" target="_blank" data-detail-link="'+detail_link+'">View on AliExpress</a>' +
                    '</div>';
            }
        }
        catch (exception){
            detail_block = '';
        }

        if(AspLocalization.asp_search_shopify_link && image_url)
        {
            //internal-link
            search_by_image_link = AspLocalization.asp_search_by_image_page + '?img_url=' + encodeURIComponent(image_url);
            search_by_image_block = '<div class="search-by-image">' +
                    '<a class="search-by-image-link btn" href="'+search_by_image_link+'" target="_blank" data-img-url="'+image_url+'">Compare Prices</a>' +
                '</div>';

            //external-link
            search_shopify_link = AspLocalization.asp_search_shopify_link + '?img_url=' + encodeURIComponent(image_url);
            search_shopify_block = '<div class="search-shopify-store">' +
                    '<a class="shopify-store-link btn" href="'+search_shopify_link+'" target="_blank" data-img-url="'+image_url+'">Find Stores Selling This</a>' +
                '</div>';
        }

        if(AspLocalization.asp_action_change_cookied)
        {
            cookied_block = "<div class='col-2 align-self-start cookied-container'>" +
                    "<i class='fa icon-cookied"+(item.cookied ? "" : "-o")+" cookied-mark' aria-hidden='true' data-id='"+item.id+"' data-guid='"+guid+"'></i>" +
                "</div>";
        }

        try{
            if(image_url){
                image = "<img src='"+ image_url +"' alt='"+ item.title +"' class='post-image' data-listing-id='"+item.id+"'>";
                img_src = image_url;
            }else{
                image = "<img src='"+ AspLocalization.asp_url_no_img +"' alt='"+ item.title +"' class='post-image' data-listing-id='"+item.id+"'>";
                img_src = AspLocalization.asp_url_no_img;
            }
        }catch (exception){
            image = "<img src='"+ AspLocalization.asp_url_no_img +"' alt='"+ item.title +"' class='post-image' data-listing-id='"+item.id+"'>";
            img_src = AspLocalization.asp_url_no_img;
        }

        //lot
        try{
            lot = parseInt(item.details.countPerLot) || 0;

            if(lot > 1){
                lotUnit = item.details.multiUnit || '';
            }else{
                lotUnit = item.details.unit || '';
            }
            lot_html_block = '<div class="row ml-0 mr-0">'
                    +'<div class="col">'
                        +'<span class="social-actions lot-container">'
                            +'<span class="lot-unit">'+ (lot + " " + lotUnit + " / lot") +'</span>'
                            +'<span class="lot-size" style="display: none;">'+ lot +'</span>'
                        +'</span>'
                    +'</div>'
                +'</div>';

        }catch(exception){
            lot = 0;
            lotUnit = '';
            lot_html_block = '';
        }

        //price
        try{
            price = (Math.round(item.price.value * 100) / 100).toFixed(2);
            if(unit_prices && lot > 1)
            {
                price = (price / lot).toFixed(2);
                lotUnit = item.details.unit || '';
                lot_unit_html = '<span class="lot-unit">'+ (" / " + lotUnit) +'</span>';
            }
        }catch(exception){
            try{
                price = (Math.round(item.priceOptions[0].amount.value * 100) / 100).toFixed(2);
                if(unit_prices && lot > 1)
                {
                    price = (price / lot).toFixed(2);
                    lotUnit = item.details.unit || '';
                    lot_unit_html = '<span class="lot-unit">'+ (" / " + lotUnit) +'</span>';
                }
            }catch(exception){
                price = 0;
            }
        }

        //description
        try{
            description = item.title ? ((item.title).substring(0, 200) + '...' ) : '';
        }catch (exception){
            description = '';
        }

        //currency symbol
        try{
            currency_symbol = getCurrencySymbol( item.price.currency );
        }catch (exception){
            currency_symbol = '';
        }

        //shipping
        try{
            shipping = item.details.shipping[0] || {};
        }catch(exception){
            shipping = {};
        }

        try{
            shipping_price = parseFloat(shipping.amount.value) || 0;
        }catch(exception){
            shipping_price = 0;
        }

        try{
            shipping_type = shipping.company;
            if(shipping_price)
            {
                shipping_num = getShippingNumByType(shipping_type);
                if(unit_prices && lot > 1)
                {
                    shipping_price = shipping_price / lot;
                    shipping_price = shipping_price > 0 ? (shipping_price).toFixed(2) : shipping_price;
                }
                shipping_out = 'Shipping: '+currency_symbol+shipping_price+' via '+shipping_type;
                shipping_class = '';
            }
            else
            {
                shipping_out = defaultFreeShippingType;
            }
        }catch(exception){
            shipping_out = defaultFreeShippingType;
            console.log('Shipping type exception!');
            console.log(exception);
        }

        //ratings
        try{
            ratings = parseFloat(item.details.reviews.ratings) || 0;
            ratings_title = 'title="'+ ratings +' Ratings"';
        }catch(exception){
            ratings = 0;
            ratings_title = '';
        }

        var tmp = '<div class="col-md-4 col-sm-6 col-xs-6 col-xl-3 element-item" data-listing-id="'+item.id+'" data-guid="'+guid+'">'
            +'<div class="box">'
                +'<div class="row">'
                    +'<div class="col">'
                        +'<div class="image-block">'
                            +'<div class="bg-wrap" style="background-image: url('+ img_src +');">'
                                +image
                            +'</div>'
                            +'<div class="button-block row align-items-center no-gutters">'
                                +'<div class="col-12">'
                                    +detail_block
                                    +search_by_image_block
                                    +search_shopify_block
                                +'</div>'
                            +'</div>'
                        +'</div>'
                    +'</div>'
                +'</div>'
                +'<div class="row ml-0 mr-0 line-3 bt-1 align-items-center no-gutters">'
                    +'<div class="col-4">'
                        /* Price */
                        +'<span class="social-actions line price-container">'
                            +'<i class="fa fa-usd" aria-hidden="true"></i>'
                            +'<span class="price" data-price-currency="'+currency_symbol+'">'+ price +'</span>'
                            /*+ lot_unit_html*/
                        +'</span>'
                        /* Best Match */
                        +'<span class="best">'+ guid +'</span>'
                    +'</div>'
                    +'<div class="col-5">'
                        /* Rating */
                        +'<span class="social-actions ratings" data-toggle="tooltip" data-placement="bottom" data-ratings="'+ ratings +'" '+ ratings_title +'>'
                            + getRatingStarsHtml(ratings)
                        +'</span>'
                    +'</div>'
                    +'<div class="col-3">'
                        /* Orders */
                        +'<span class="social-actions orders-container" data-toggle="tooltip" data-placement="bottom" title="'+ item.orders +' Orders">'
                            +'<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shopping-bag" class="svg-inline--fa fa-shopping-bag fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="20">'
                                +'<path fill="currentColor" d="M352 160v-32C352 57.42 294.579 0 224 0 153.42 0 96 57.42 96 128v32H0v272c0 44.183 35.817 80 80 80h288c44.183 0 80-35.817 80-80V160h-96zm-192-32c0-35.29 28.71-64 64-64s64 28.71 64 64v32H160v-32zm160 120c-13.255 0-24-10.745-24-24s10.745-24 24-24 24 10.745 24 24-10.745 24-24 24zm-192 0c-13.255 0-24-10.745-24-24s10.745-24 24-24 24 10.745 24 24-10.745 24-24 24z"></path>'
                            +'</svg>'
                            +'<span class="orders">'+ item.orders +'</span>'
                        +'</span>'
                    +'</div>'
                +'</div>'
                +'<div class="row ml-0 mr-0 line-3 bt-1 dis-flex align-content-center">'
                    +'<div class="col align-self-center">'
                        /* Shipping */
                        +'<div class="post-text">'
                            +'<span class="shipping '+shipping_class+'" data-shipping="'+ shipping_num +'" data-shipping-type="'+ shipping_type +'">'+ shipping_out +'</span>'
                        +'</div>'
                    +'</div>'
                +'</div>'
                +'<div class="row ml-0 mr-0 line-6 bt-1 dis-flex align-content-center">'
                    +'<div class="col-'+(cookied_block ? '10' : '12')+' align-self-center">'
                        +'<div class="post-text">'
                            +'<a href="'+detail_link+'" target="_blank" class="post-text-inner">'+ description +'</a>'
                        +'</div>'
                    +'</div>'
                    +cookied_block
                +'</div>'
            +'</div>'
        +'</div>';

        return object ? $(tmp) : tmp;
    }

    /**
     * Clear File Input
     */
    function clearFileInput()
    {
        clearFileInputById('file-input-label');//put ID label !
    }

    /**
     * Clear File Input
     * https://habr.com/ru/post/65687/
     * https://habr.com/ru/post/65687/#comment_1840562
     *
     * @param Id - ID label for me!
     */
    function clearFileInputById(Id)
    {
       document.getElementById(Id).innerHTML = document.getElementById(Id).innerHTML;
    }


    /**
     * Function send ajax multipart request
     *
     * @param params
     */
    function send_ajax_multipart(params)
    {
        var options = {
            "async": true,
            "crossDomain": true,
            "method": 'POST',
            "headers": {},
            callback: function () {},
            callback_error: function () {}
        };

        jQuery.extend(options, params);

        jQuery.ajax({
            headers: options.headers,
            url:  options.url,
            data: options.data,
            async: options.async,
            crossDomain: options.crossDomain,
            type: options.method,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (result) {
                if(typeof options.callback == 'function'){
                    options.callback(result);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('function send_ajax_multipart error!');
                if(typeof options.callback_error == 'function'){
                    options.callback_error(jqXHR);
                }
            }
        });
    }
});