<?php
/**
 * The template page for search by image URL
 */

global $post;
get_header();

$title_page = get_the_title($post->ID);

if( is_user_logged_in() )
{
    $current_user = wp_get_current_user();
    $email        = $current_user->user_email;
    $username     = get_user_meta( $current_user->ID, 'user_name', true );
    $username     = $username ? $username : $email;
    $userid       = get_user_meta( $current_user->ID, 'user_id', true );

    ?>

    <script type="text/javascript">
        window.mixpanel.identify("<?php echo $email; ?>");
        window.mixpanel.people.set({
            "$name": "<?php echo $username ?>",
            "$email": "<?php echo $email; ?>",
            "$created": "<?php echo $current_user->user_registered; ?>",
        });
    </script>

    <?php
}
?>

    <section class="content main-aliexpress-search aliexpress-search-by-image-url">

        <div class="page-header">
            <h1 class="page-title"><?php echo $title_page; ?></h1>
            <script type="text/javascript">
                if(window.AspLocalization.asp_production){
                    window.mixpanel.track( "Viewed: " + (document.getElementsByClassName("page-title")[0]).textContent + " page" );
                }
            </script>
        </div>

        <div class="container">

            <div class="row row justify-content-end align-items-center pt-4">

                <div class="col">
                    <div class="row justify-content-end align-items-center">

                        <div class="col aliexpress-sort-container">
                            <label>Sort By</label>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-default-text="Best Match" data-default-sort="best" data-sort="best">
                                    Best Match
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuSortBy">
                                    <a class="dropdown-item aliexpress-sort active" data-sort="best" href="#">Best Match</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="ratings" href="#">Ratings</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="ordersAsc" href="#">▲ Orders</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="ordersDesc" href="#">▼ Orders</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="priceAsc" href="#">▲ Price</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="priceDesc" href="#">▼ Price</a>
                                </div>
                            </div>
                        </div>

                        <div class="col aliexpress-ship-filter-container">
                            <label>Shipping</label>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuShipFilterBy" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-default-text="Any Type" data-default-filter="0" data-filter="0">
                                    Any Type
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuShipFilterBy">
                                    <a class="dropdown-item aliexpress-ship-filter active" data-filter="0" href="#" title="Any Type">Any Type</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="1" href="#" title="AliExpress Standard Shipping">AESS</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="2" href="#" title="China Post Ordinary Small Packet Plus">CPOSPP</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="3" href="#" title="ePacket">ePacket</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="4" href="#" title="DHL">DHL</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="5" href="#" title="EMS">EMS</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="6" href="#" title="Yanwen Economic Air Mail">YEAM</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="7" href="#" title="Free Shipping" id="aliexpress-item-filter-free">FREE</a>
                                </div>
                            </div>
                        </div>

                        <div class="col aliexpress-reset-filter">
                            <button type="button" class="btn btn-outline-danger aliexpress-reset-filter-btn">Reset Filters&nbsp;<i class="fa fa-ban" aria-hidden="true"></i></button>
                        </div>

                    </div>
                </div>

                <div class="col-lx-6 col-12 aliexpress-filter-result">
                    <p class="aliexpress-filter-result-text">Showing <span id="current-show-filter-result">0</span> of <span id="total-filter-result">0</span> items.</p>
                </div>

            </div><!-- /.aliexpress-filters -->

            <div class="row">
                <div class="col-12 aliexpress-out-msg">
                    <div class="aliexpress-preloader aliexpress-preloader-top">
                        <svg class="lds-spinner" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><g transform="rotate(0 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-1.7416666666666665s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(30 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-1.5833333333333333s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(60 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-1.4249999999999998s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(90 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-1.2666666666666666s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(120 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-1.1083333333333332s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(150 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-0.9499999999999998s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(180 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-0.7916666666666666s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(210 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-0.6333333333333333s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(240 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-0.4749999999999999s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(270 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-0.31666666666666665s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(300 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="-0.15833333333333333s" repeatCount="indefinite"></animate>
                                </rect>
                            </g><g transform="rotate(330 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="0s" repeatCount="indefinite"></animate>
                                </rect>
                            </g></svg>
                        <span class="sr-only">Loading...</span>
                    </div>

                    <div class="aliexpress-no-results pt-3">No results.</div>
                    <div class="aliexpress-results-error pt-3"></div>
                </div>
            </div><!-- /.row -->

            <div id="aliexpress-listing" class="row"></div><!-- /#aliexpress-listing -->

            <div id="pageup">
                <i class="fa fa-caret-square-o-up" aria-hidden="true"></i>
                <span class="sr-only">Page up</span>
            </div>

        </div><!-- /.container -->

    </section><!-- /.main -->

<?php

get_footer();