<?php
/**
 * The page template
 */

global $post;
get_header();

$title_page = get_the_title($post->ID);

if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $email = $current_user->user_email;
    $username = get_user_meta($current_user->ID, 'user_name', true);
    $username = $username ? $username : $email;
    $userid = get_user_meta($current_user->ID, 'user_id', true);

    ?>

    <script type="text/javascript">
        window.mixpanel.identify("<?php echo $email; ?>");
        window.mixpanel.people.set({
            "$name": "<?php echo $username ?>",
            "$email": "<?php echo $email; ?>",
            "$created": "<?php echo $current_user->user_registered; ?>",
        });
    </script>

    <?php
}
?>

    <section class="content main-aliexpress-search">

        <div class="container">

            <div class="page-header">
                <h1 class="page-title"><?php echo $title_page; ?></h1>
                <script type="text/javascript">
                    if (window.AspLocalization.asp_production) {
                        window.mixpanel.track("Viewed: " + (document.getElementsByClassName("page-title")[0]).textContent + " page");
                    }
                </script>
            </div>

            <div class="row search-box">
                <div class="col-12 aliexpress-search-container">
                    <form id="aliexpress-search" class="row no-gutters align-items-stretch aliexpress-search">

                        <div class="col-12">
                            <div class="row mb-2r">
                                <div class="input-group mb-0 mr-sm-0 mb-sm-0 pr-0 col">
                                    <label id="file-input-label" class="file-input-label row align-items-center no-gutters" for="file-input" title="Search by images">
                                        <input type="file" class="file-input" id="file-input">
                                        <i class="fa fa-camera-retro" aria-hidden="true"></i>
                                    </label>
                                    <input type="text" name="aliexpress_keywords" id="aliexpress-keywords" class="aliexpress-keywords" placeholder="Enter some keyword(s)" autocomplete="off" required>
                                </div>
                                <div class="col-md-3 pl-0 btn-column">
                                    <button type="submit" class="btn btn-primary aliexpress-submit">Search</button>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="row filter-box">

                                <div class="col-md-6 col-lg-3 col-xl-3 aliexpress-form-sort-price-container">
                                    <div class="row flex-col mb-2r box-shadow">
                                        <label class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 col-form-label display-none">Price</label>

                                        <div class="input-group col-12">
                                            <div class="aliexpress-fa-icon money">
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                            </div>
                                            <input type="text" class="form-control first-child-element" name="price_min" id="price-min" step="1" min="0" placeholder="Min Price">
                                        </div>

                                        <div class="input-group col-12">
                                            <div class="aliexpress-fa-icon money">
                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                            </div>
                                            <input type="text" class="form-control second-child-element" name="price_max" id="price-max" step="1" min="0" placeholder="Max Price">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-3 col-xl-3 aliexpress-form-sort-rating-container">
                                    <div class="row flex-col mb-2r box-shadow">
                                        <label class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 col-form-label display-none">Rating</label>

                                        <div class="input-group col-12">
                                            <div class="aliexpress-fa-icon">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                            </div>
                                            <input type="text" class="form-control first-child-element" name="rating_min" id="rating-min" step="0.1" min="0" placeholder="Min Rating">
                                        </div>

                                        <div class="input-group col-12">
                                            <div class="aliexpress-fa-icon">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                            </div>
                                            <input type="text" class="form-control second-child-element" name="rating_max" id="rating-max" step="0.1" min="0" placeholder="Max Rating">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-3 col-xl-3 aliexpress-form-sort-quantity-container">
                                    <div class="row flex-col mb-2r box-shadow">
                                        <label class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 col-form-label display-none">Quantity</label>

                                        <div class="input-group col-12">
                                            <div class="aliexpress-fa-icon quantity">
                                                <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                     data-icon="boxes" class="svg-inline--fa fa-boxes fa-w-18"
                                                     role="img" xmlns="http://www.w3.org/2000/svg"
                                                     viewBox="0 0 576 512" width="25">
                                                    <path fill="currentColor"
                                                          d="M560 288h-80v96l-32-21.3-32 21.3v-96h-80c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16zm-384-64h224c8.8 0 16-7.2 16-16V16c0-8.8-7.2-16-16-16h-80v96l-32-21.3L256 96V0h-80c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16zm64 64h-80v96l-32-21.3L96 384v-96H16c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16z"></path>
                                                </svg>
                                            </div>
                                            <input type="text" class="form-control first-child-element" name="quantity_min" id="quantity-min" step="1" min="0" placeholder="Min Quantity">
                                        </div>

                                        <div class="input-group col-12">
                                            <div class="aliexpress-fa-icon quantity">
                                                <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                     data-icon="boxes" class="svg-inline--fa fa-boxes fa-w-18"
                                                     role="img" xmlns="http://www.w3.org/2000/svg"
                                                     viewBox="0 0 576 512" width="25">
                                                    <path fill="currentColor"
                                                          d="M560 288h-80v96l-32-21.3-32 21.3v-96h-80c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16zm-384-64h224c8.8 0 16-7.2 16-16V16c0-8.8-7.2-16-16-16h-80v96l-32-21.3L256 96V0h-80c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16zm64 64h-80v96l-32-21.3L96 384v-96H16c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16z"></path>
                                                </svg>
                                            </div>
                                            <input type="text" class="form-control second-child-element" name="quantity_max" id="quantity-max" step="1" min="0" placeholder="Max Quantity">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-3 col-xl-3 aliexpress-form-sort-order-container">
                                    <div class="row flex-col mb-2r box-shadow">
                                        <label class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 col-form-label display-none">Orders</label>

                                        <div class="input-group col-12">
                                            <div class="aliexpress-fa-icon">
                                                <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                     data-icon="shipping-fast"
                                                     class="svg-inline--fa fa-shipping-fast fa-w-20" role="img"
                                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" width="25">
                                                    <path fill="currentColor"
                                                          d="M624 352h-16V243.9c0-12.7-5.1-24.9-14.1-33.9L494 110.1c-9-9-21.2-14.1-33.9-14.1H416V48c0-26.5-21.5-48-48-48H112C85.5 0 64 21.5 64 48v48H8c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h272c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H40c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h208c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H8c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h208c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H64v128c0 53 43 96 96 96s96-43 96-96h128c0 53 43 96 96 96s96-43 96-96h48c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zM160 464c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm320 0c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm80-208H416V144h44.1l99.9 99.9V256z"></path>
                                                </svg>
                                            </div>
                                            <input type="text" class="form-control first-child-element" name="order_min" id="order-min" step="1" min="0" placeholder="Min Orders">
                                        </div>

                                        <div class="input-group col-12">
                                            <div class="aliexpress-fa-icon">
                                                <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                     data-icon="shipping-fast"
                                                     class="svg-inline--fa fa-shipping-fast fa-w-20" role="img"
                                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" width="25">
                                                    <path fill="currentColor"
                                                          d="M624 352h-16V243.9c0-12.7-5.1-24.9-14.1-33.9L494 110.1c-9-9-21.2-14.1-33.9-14.1H416V48c0-26.5-21.5-48-48-48H112C85.5 0 64 21.5 64 48v48H8c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h272c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H40c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h208c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H8c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h208c4.4 0 8 3.6 8 8v16c0 4.4-3.6 8-8 8H64v128c0 53 43 96 96 96s96-43 96-96h128c0 53 43 96 96 96s96-43 96-96h48c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zM160 464c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm320 0c-26.5 0-48-21.5-48-48s21.5-48 48-48 48 21.5 48 48-21.5 48-48 48zm80-208H416V144h44.1l99.9 99.9V256z"></path>
                                                </svg>
                                            </div>
                                            <input type="text" class="form-control second-child-element" name="order_max" id="order-max" step="1" min="0" placeholder="Max Orders">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-12" style="display: none;">
                            <div class="row">
                                <div class="check-box">
                                    <div class="col-12 aliexpress-form-check-container mb-3">
                                        <div class="row form-group">
                                            <input type="checkbox" class="form-check-input" name="unit_prices" id="unit-prices"/>
                                            <label class="form-check-label" for="unit-prices">Show Unit Prices</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div><!-- /.row -->

            <div class="row">
                <div class="col-12 aliexpress-out-msg">
                    <div class="aliexpress-preloader aliexpress-preloader-top">
                        <svg class="lds-spinner" width="200px" height="200px" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100"
                             preserveAspectRatio="xMidYMid" style="background: none;">
                            <g transform="rotate(0 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-1.7416666666666665s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(30 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-1.5833333333333333s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(60 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-1.4249999999999998s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(90 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-1.2666666666666666s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(120 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-1.1083333333333332s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(150 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-0.9499999999999998s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(180 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-0.7916666666666666s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(210 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-0.6333333333333333s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(240 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-0.4749999999999999s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(270 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-0.31666666666666665s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(300 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s"
                                             begin="-0.15833333333333333s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(330 50 50)">
                                <rect x="47" y="24" rx="9.4" ry="4.8" width="6" height="12" fill="#734cd1">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1.9s" begin="0s"
                                             repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                        </svg>
                        <span class="sr-only">Loading...</span>
                    </div>

                    <div class="aliexpress-results-end">Here are <span class="aliexpress-count-result"></span> results below.<br/>Results end!</div>
                    <div class="aliexpress-no-results">No results.</div>
                    <div class="aliexpress-results-error"></div>
                </div>
            </div><!-- /.row -->

            <div class="row align-items-center aliexpress-filters">

                <div class="col">
                    <div class="row justify-content-end align-items-center">

                        <div class="col aliexpress-sort-container">
                            <label>Sort By</label>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuSortBy" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-default-text="Best Match" data-default-sort="best" data-sort="best">
                                    Best Match
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuSortBy">
                                    <a class="dropdown-item aliexpress-sort active" data-sort="best" href="#">Best Match</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="ratings" href="#">Ratings</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="ordersAsc" href="#">▲ Orders</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="ordersDesc" href="#">▼ Orders</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="priceAsc" href="#">▲ Price</a>
                                    <a class="dropdown-item aliexpress-sort" data-sort="priceDesc" href="#">▼ Price</a>
                                </div>
                            </div>
                        </div>

                        <div class="col aliexpress-ship-filter-container">
                            <label>Shipping</label>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuShipFilterBy" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-default-text="Any Type" data-default-filter="0" data-filter="0">
                                    Any Type
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuShipFilterBy">
                                    <a class="dropdown-item aliexpress-ship-filter active" data-filter="0" href="#" title="Any Type">Any Type</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="1" href="#" title="AliExpress Standard Shipping">AESS</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="2" href="#" title="China Post Ordinary Small Packet Plus">CPOSPP</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="3" href="#" title="ePacket">ePacket</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="4" href="#" title="DHL">DHL</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="5" href="#" title="EMS">EMS</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="6" href="#" title="Yanwen Economic Air Mail">YEAM</a>
                                    <a class="dropdown-item aliexpress-ship-filter" data-filter="7" href="#" title="Free Shipping" id="aliexpress-item-filter-free">FREE</a>
                                </div>
                            </div>
                        </div>

                        <div class="col aliexpress-reset-filter">
                            <button type="button" class="btn btn-outline-danger aliexpress-reset-filter-btn">Reset Filters&nbsp;<i class="fa fa-ban" aria-hidden="true"></i></button>
                        </div>

                    </div>
                </div>

                <div class="col-lx-8 col-12 aliexpress-filter-result">
                    <p class="aliexpress-filter-result-text">Showing <span id="current-show-filter-result">0</span> of <span id="total-filter-result">0</span> items.</p>
                    <div class="aliexpress-more-result"> Note: Some are hidden. <a href="#" class="aliexpress-load-more">Click here to upload more results.</a></div>
                </div>

            </div><!-- /.aliexpress-filters -->

            <div id="aliexpress-listing" class="row"></div><!-- /#aliexpress-listing -->

            <div class="row">
                <div class="col-12 aliexpress-out-msg">
                    <div class="aliexpress-preloader aliexpress-preloader-bottom">
                        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>

                    <div class="aliexpress-results-end">Here are <span class="aliexpress-count-result"></span> results above.<br/>Results end!</div>

                    <div class="aliexpress-more-result">
                        <p class="aliexpress-filter-result-text">Showing <span id="current-show-more-result">0</span> of <span id="total-more-result">0</span> items.</p>
                        <div class="aliexpress-more-result">Note: Some are hidden. <a href="#" class="aliexpress-load-more">Click here to upload more results.</a></div>
                    </div>
                </div>
            </div>

            <div id="pageup">
                <i class="fa fa-caret-square-o-up" aria-hidden="true"></i>
                <span class="sr-only">Page up</span>
            </div>

        </div><!-- /.container -->
    </section><!-- /.main -->

<?php

get_footer();